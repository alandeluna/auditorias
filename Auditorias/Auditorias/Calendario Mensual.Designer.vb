﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Calendario_Mensual
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Tbl_Lay = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel42 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_42 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel42 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_42 = New System.Windows.Forms.Label()
        Me.Panel41 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_41 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel41 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_41 = New System.Windows.Forms.Label()
        Me.Panel40 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_40 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel40 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_40 = New System.Windows.Forms.Label()
        Me.Panel39 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_39 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel39 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_39 = New System.Windows.Forms.Label()
        Me.Panel38 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_38 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel38 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_38 = New System.Windows.Forms.Label()
        Me.Panel37 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_37 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel37 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_37 = New System.Windows.Forms.Label()
        Me.Panel36 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_36 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel36 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_36 = New System.Windows.Forms.Label()
        Me.Panel35 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_35 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel34 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_35 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_3 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_4 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_4 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_8 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_8 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_9 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_9 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_10 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_10 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_11 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_11 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_15 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel15 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_15 = New System.Windows.Forms.Label()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_16 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel16 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_16 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_17 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel17 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_17 = New System.Windows.Forms.Label()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_18 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel18 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_18 = New System.Windows.Forms.Label()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_5 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_5 = New System.Windows.Forms.Label()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_6 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_6 = New System.Windows.Forms.Label()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_7 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_7 = New System.Windows.Forms.Label()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_12 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel12 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_12 = New System.Windows.Forms.Label()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_13 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel13 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_13 = New System.Windows.Forms.Label()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_14 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel14 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_14 = New System.Windows.Forms.Label()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_19 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel19 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_19 = New System.Windows.Forms.Label()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_20 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel20 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_20 = New System.Windows.Forms.Label()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_21 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel21 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_21 = New System.Windows.Forms.Label()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_22 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel22 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_22 = New System.Windows.Forms.Label()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_23 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel23 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_23 = New System.Windows.Forms.Label()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_24 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel24 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_24 = New System.Windows.Forms.Label()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_25 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel25 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_25 = New System.Windows.Forms.Label()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_26 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel26 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_26 = New System.Windows.Forms.Label()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_27 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel27 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_27 = New System.Windows.Forms.Label()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_28 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel28 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_28 = New System.Windows.Forms.Label()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_29 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel29 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_29 = New System.Windows.Forms.Label()
        Me.Panel30 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_30 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel30 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_30 = New System.Windows.Forms.Label()
        Me.Panel31 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_31 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel35 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_31 = New System.Windows.Forms.Label()
        Me.Panel32 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_32 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel31 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_32 = New System.Windows.Forms.Label()
        Me.Panel33 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_33 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel32 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_33 = New System.Windows.Forms.Label()
        Me.Panel34 = New System.Windows.Forms.Panel()
        Me.Tbl_Lay_34 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel33 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lbl_34 = New System.Windows.Forms.Label()
        Me.Tbl_Lay.SuspendLayout()
        Me.Panel42.SuspendLayout()
        Me.Tbl_Lay_42.SuspendLayout()
        Me.Panel41.SuspendLayout()
        Me.Tbl_Lay_41.SuspendLayout()
        Me.Panel40.SuspendLayout()
        Me.Tbl_Lay_40.SuspendLayout()
        Me.Panel39.SuspendLayout()
        Me.Tbl_Lay_39.SuspendLayout()
        Me.Panel38.SuspendLayout()
        Me.Tbl_Lay_38.SuspendLayout()
        Me.Panel37.SuspendLayout()
        Me.Tbl_Lay_37.SuspendLayout()
        Me.Panel36.SuspendLayout()
        Me.Tbl_Lay_36.SuspendLayout()
        Me.Panel35.SuspendLayout()
        Me.Tbl_Lay_35.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Tbl_Lay_1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Tbl_Lay_2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Tbl_Lay_3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Tbl_Lay_4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Tbl_Lay_8.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Tbl_Lay_9.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Tbl_Lay_10.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Tbl_Lay_11.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Tbl_Lay_15.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Tbl_Lay_16.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Tbl_Lay_17.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Tbl_Lay_18.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Tbl_Lay_5.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Tbl_Lay_6.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Tbl_Lay_7.SuspendLayout()
        Me.Panel16.SuspendLayout()
        Me.Tbl_Lay_12.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Tbl_Lay_13.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.Tbl_Lay_14.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Tbl_Lay_19.SuspendLayout()
        Me.Panel20.SuspendLayout()
        Me.Tbl_Lay_20.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Tbl_Lay_21.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Tbl_Lay_22.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.Tbl_Lay_23.SuspendLayout()
        Me.Panel24.SuspendLayout()
        Me.Tbl_Lay_24.SuspendLayout()
        Me.Panel25.SuspendLayout()
        Me.Tbl_Lay_25.SuspendLayout()
        Me.Panel26.SuspendLayout()
        Me.Tbl_Lay_26.SuspendLayout()
        Me.Panel27.SuspendLayout()
        Me.Tbl_Lay_27.SuspendLayout()
        Me.Panel28.SuspendLayout()
        Me.Tbl_Lay_28.SuspendLayout()
        Me.Panel29.SuspendLayout()
        Me.Tbl_Lay_29.SuspendLayout()
        Me.Panel30.SuspendLayout()
        Me.Tbl_Lay_30.SuspendLayout()
        Me.Panel31.SuspendLayout()
        Me.Tbl_Lay_31.SuspendLayout()
        Me.Panel32.SuspendLayout()
        Me.Tbl_Lay_32.SuspendLayout()
        Me.Panel33.SuspendLayout()
        Me.Tbl_Lay_33.SuspendLayout()
        Me.Panel34.SuspendLayout()
        Me.Tbl_Lay_34.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tbl_Lay
        '
        Me.Tbl_Lay.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Tbl_Lay.ColumnCount = 7
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.Tbl_Lay.Controls.Add(Me.Panel42, 6, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel41, 5, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel40, 4, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel39, 3, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel38, 2, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel37, 1, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel36, 0, 5)
        Me.Tbl_Lay.Controls.Add(Me.Panel35, 6, 4)
        Me.Tbl_Lay.Controls.Add(Me.Panel1, 0, 0)
        Me.Tbl_Lay.Controls.Add(Me.Panel2, 1, 0)
        Me.Tbl_Lay.Controls.Add(Me.Panel3, 2, 0)
        Me.Tbl_Lay.Controls.Add(Me.Panel4, 3, 0)
        Me.Tbl_Lay.Controls.Add(Me.Panel5, 0, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel6, 1, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel7, 2, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel8, 3, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel9, 0, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel10, 1, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel11, 2, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel12, 3, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel13, 4, 0)
        Me.Tbl_Lay.Controls.Add(Me.Panel14, 5, 0)
        Me.Tbl_Lay.Controls.Add(Me.Panel15)
        Me.Tbl_Lay.Controls.Add(Me.Panel16, 4, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel17, 5, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel18, 6, 1)
        Me.Tbl_Lay.Controls.Add(Me.Panel19, 4, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel20, 5, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel21, 6, 2)
        Me.Tbl_Lay.Controls.Add(Me.Panel22, 0, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel23, 1, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel24, 2, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel25, 3, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel26, 4, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel27, 5, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel28, 6, 3)
        Me.Tbl_Lay.Controls.Add(Me.Panel29, 0, 4)
        Me.Tbl_Lay.Controls.Add(Me.Panel30, 1, 4)
        Me.Tbl_Lay.Controls.Add(Me.Panel31, 2, 4)
        Me.Tbl_Lay.Controls.Add(Me.Panel32, 3, 4)
        Me.Tbl_Lay.Controls.Add(Me.Panel33, 4, 4)
        Me.Tbl_Lay.Controls.Add(Me.Panel34, 5, 4)
        Me.Tbl_Lay.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay.Name = "Tbl_Lay"
        Me.Tbl_Lay.RowCount = 6
        Me.Tbl_Lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.Tbl_Lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.Tbl_Lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.Tbl_Lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.Tbl_Lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.Tbl_Lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.Tbl_Lay.Size = New System.Drawing.Size(784, 562)
        Me.Tbl_Lay.TabIndex = 1
        '
        'Panel42
        '
        Me.Panel42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel42.Controls.Add(Me.Tbl_Lay_42)
        Me.Panel42.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel42.Location = New System.Drawing.Point(669, 468)
        Me.Panel42.Name = "Panel42"
        Me.Panel42.Size = New System.Drawing.Size(112, 91)
        Me.Panel42.TabIndex = 41
        '
        'Tbl_Lay_42
        '
        Me.Tbl_Lay_42.ColumnCount = 1
        Me.Tbl_Lay_42.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_42.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_42.Controls.Add(Me.FlowLayoutPanel42, 0, 1)
        Me.Tbl_Lay_42.Controls.Add(Me.lbl_42, 0, 0)
        Me.Tbl_Lay_42.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_42.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_42.Name = "Tbl_Lay_42"
        Me.Tbl_Lay_42.RowCount = 2
        Me.Tbl_Lay_42.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_42.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_42.Size = New System.Drawing.Size(108, 87)
        Me.Tbl_Lay_42.TabIndex = 2
        Me.Tbl_Lay_42.Visible = False
        '
        'FlowLayoutPanel42
        '
        Me.FlowLayoutPanel42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel42.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel42.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel42.Name = "FlowLayoutPanel42"
        Me.FlowLayoutPanel42.Size = New System.Drawing.Size(102, 38)
        Me.FlowLayoutPanel42.TabIndex = 2
        '
        'lbl_42
        '
        Me.lbl_42.AutoSize = True
        Me.lbl_42.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_42.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_42.Location = New System.Drawing.Point(3, 0)
        Me.lbl_42.Name = "lbl_42"
        Me.lbl_42.Size = New System.Drawing.Size(102, 43)
        Me.lbl_42.TabIndex = 0
        Me.lbl_42.Text = "42"
        Me.lbl_42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel41
        '
        Me.Panel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel41.Controls.Add(Me.Tbl_Lay_41)
        Me.Panel41.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel41.Location = New System.Drawing.Point(558, 468)
        Me.Panel41.Name = "Panel41"
        Me.Panel41.Size = New System.Drawing.Size(105, 91)
        Me.Panel41.TabIndex = 40
        '
        'Tbl_Lay_41
        '
        Me.Tbl_Lay_41.ColumnCount = 1
        Me.Tbl_Lay_41.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_41.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_41.Controls.Add(Me.FlowLayoutPanel41, 0, 1)
        Me.Tbl_Lay_41.Controls.Add(Me.lbl_41, 0, 0)
        Me.Tbl_Lay_41.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_41.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_41.Name = "Tbl_Lay_41"
        Me.Tbl_Lay_41.RowCount = 2
        Me.Tbl_Lay_41.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_41.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_41.Size = New System.Drawing.Size(101, 87)
        Me.Tbl_Lay_41.TabIndex = 2
        Me.Tbl_Lay_41.Visible = False
        '
        'FlowLayoutPanel41
        '
        Me.FlowLayoutPanel41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel41.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel41.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel41.Name = "FlowLayoutPanel41"
        Me.FlowLayoutPanel41.Size = New System.Drawing.Size(95, 38)
        Me.FlowLayoutPanel41.TabIndex = 2
        '
        'lbl_41
        '
        Me.lbl_41.AutoSize = True
        Me.lbl_41.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_41.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_41.Location = New System.Drawing.Point(3, 0)
        Me.lbl_41.Name = "lbl_41"
        Me.lbl_41.Size = New System.Drawing.Size(95, 43)
        Me.lbl_41.TabIndex = 0
        Me.lbl_41.Text = "41"
        Me.lbl_41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel40
        '
        Me.Panel40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel40.Controls.Add(Me.Tbl_Lay_40)
        Me.Panel40.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel40.Location = New System.Drawing.Point(447, 468)
        Me.Panel40.Name = "Panel40"
        Me.Panel40.Size = New System.Drawing.Size(105, 91)
        Me.Panel40.TabIndex = 39
        '
        'Tbl_Lay_40
        '
        Me.Tbl_Lay_40.ColumnCount = 1
        Me.Tbl_Lay_40.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_40.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_40.Controls.Add(Me.FlowLayoutPanel40, 0, 1)
        Me.Tbl_Lay_40.Controls.Add(Me.lbl_40, 0, 0)
        Me.Tbl_Lay_40.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_40.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_40.Name = "Tbl_Lay_40"
        Me.Tbl_Lay_40.RowCount = 2
        Me.Tbl_Lay_40.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_40.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_40.Size = New System.Drawing.Size(101, 87)
        Me.Tbl_Lay_40.TabIndex = 2
        Me.Tbl_Lay_40.Visible = False
        '
        'FlowLayoutPanel40
        '
        Me.FlowLayoutPanel40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel40.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel40.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel40.Name = "FlowLayoutPanel40"
        Me.FlowLayoutPanel40.Size = New System.Drawing.Size(95, 38)
        Me.FlowLayoutPanel40.TabIndex = 2
        '
        'lbl_40
        '
        Me.lbl_40.AutoSize = True
        Me.lbl_40.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_40.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_40.Location = New System.Drawing.Point(3, 0)
        Me.lbl_40.Name = "lbl_40"
        Me.lbl_40.Size = New System.Drawing.Size(95, 43)
        Me.lbl_40.TabIndex = 0
        Me.lbl_40.Text = "40"
        Me.lbl_40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel39
        '
        Me.Panel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel39.Controls.Add(Me.Tbl_Lay_39)
        Me.Panel39.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel39.Location = New System.Drawing.Point(336, 468)
        Me.Panel39.Name = "Panel39"
        Me.Panel39.Size = New System.Drawing.Size(105, 91)
        Me.Panel39.TabIndex = 38
        '
        'Tbl_Lay_39
        '
        Me.Tbl_Lay_39.ColumnCount = 1
        Me.Tbl_Lay_39.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_39.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_39.Controls.Add(Me.FlowLayoutPanel39, 0, 1)
        Me.Tbl_Lay_39.Controls.Add(Me.lbl_39, 0, 0)
        Me.Tbl_Lay_39.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_39.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_39.Name = "Tbl_Lay_39"
        Me.Tbl_Lay_39.RowCount = 2
        Me.Tbl_Lay_39.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_39.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_39.Size = New System.Drawing.Size(101, 87)
        Me.Tbl_Lay_39.TabIndex = 2
        Me.Tbl_Lay_39.Visible = False
        '
        'FlowLayoutPanel39
        '
        Me.FlowLayoutPanel39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel39.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel39.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel39.Name = "FlowLayoutPanel39"
        Me.FlowLayoutPanel39.Size = New System.Drawing.Size(95, 38)
        Me.FlowLayoutPanel39.TabIndex = 2
        '
        'lbl_39
        '
        Me.lbl_39.AutoSize = True
        Me.lbl_39.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_39.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_39.Location = New System.Drawing.Point(3, 0)
        Me.lbl_39.Name = "lbl_39"
        Me.lbl_39.Size = New System.Drawing.Size(95, 43)
        Me.lbl_39.TabIndex = 0
        Me.lbl_39.Text = "39"
        Me.lbl_39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel38
        '
        Me.Panel38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel38.Controls.Add(Me.Tbl_Lay_38)
        Me.Panel38.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel38.Location = New System.Drawing.Point(225, 468)
        Me.Panel38.Name = "Panel38"
        Me.Panel38.Size = New System.Drawing.Size(105, 91)
        Me.Panel38.TabIndex = 37
        '
        'Tbl_Lay_38
        '
        Me.Tbl_Lay_38.ColumnCount = 1
        Me.Tbl_Lay_38.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_38.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_38.Controls.Add(Me.FlowLayoutPanel38, 0, 1)
        Me.Tbl_Lay_38.Controls.Add(Me.lbl_38, 0, 0)
        Me.Tbl_Lay_38.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_38.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_38.Name = "Tbl_Lay_38"
        Me.Tbl_Lay_38.RowCount = 2
        Me.Tbl_Lay_38.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_38.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_38.Size = New System.Drawing.Size(101, 87)
        Me.Tbl_Lay_38.TabIndex = 2
        Me.Tbl_Lay_38.Visible = False
        '
        'FlowLayoutPanel38
        '
        Me.FlowLayoutPanel38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel38.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel38.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel38.Name = "FlowLayoutPanel38"
        Me.FlowLayoutPanel38.Size = New System.Drawing.Size(95, 38)
        Me.FlowLayoutPanel38.TabIndex = 2
        '
        'lbl_38
        '
        Me.lbl_38.AutoSize = True
        Me.lbl_38.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_38.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_38.Location = New System.Drawing.Point(3, 0)
        Me.lbl_38.Name = "lbl_38"
        Me.lbl_38.Size = New System.Drawing.Size(95, 43)
        Me.lbl_38.TabIndex = 0
        Me.lbl_38.Text = "38"
        Me.lbl_38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel37
        '
        Me.Panel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel37.Controls.Add(Me.Tbl_Lay_37)
        Me.Panel37.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel37.Location = New System.Drawing.Point(114, 468)
        Me.Panel37.Name = "Panel37"
        Me.Panel37.Size = New System.Drawing.Size(105, 91)
        Me.Panel37.TabIndex = 36
        '
        'Tbl_Lay_37
        '
        Me.Tbl_Lay_37.ColumnCount = 1
        Me.Tbl_Lay_37.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_37.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_37.Controls.Add(Me.FlowLayoutPanel37, 0, 1)
        Me.Tbl_Lay_37.Controls.Add(Me.lbl_37, 0, 0)
        Me.Tbl_Lay_37.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_37.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_37.Name = "Tbl_Lay_37"
        Me.Tbl_Lay_37.RowCount = 2
        Me.Tbl_Lay_37.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_37.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_37.Size = New System.Drawing.Size(101, 87)
        Me.Tbl_Lay_37.TabIndex = 2
        Me.Tbl_Lay_37.Visible = False
        '
        'FlowLayoutPanel37
        '
        Me.FlowLayoutPanel37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel37.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel37.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel37.Name = "FlowLayoutPanel37"
        Me.FlowLayoutPanel37.Size = New System.Drawing.Size(95, 38)
        Me.FlowLayoutPanel37.TabIndex = 2
        '
        'lbl_37
        '
        Me.lbl_37.AutoSize = True
        Me.lbl_37.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_37.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_37.Location = New System.Drawing.Point(3, 0)
        Me.lbl_37.Name = "lbl_37"
        Me.lbl_37.Size = New System.Drawing.Size(95, 43)
        Me.lbl_37.TabIndex = 0
        Me.lbl_37.Text = "37"
        Me.lbl_37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel36
        '
        Me.Panel36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel36.Controls.Add(Me.Tbl_Lay_36)
        Me.Panel36.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel36.Location = New System.Drawing.Point(3, 468)
        Me.Panel36.Name = "Panel36"
        Me.Panel36.Size = New System.Drawing.Size(105, 91)
        Me.Panel36.TabIndex = 35
        '
        'Tbl_Lay_36
        '
        Me.Tbl_Lay_36.ColumnCount = 1
        Me.Tbl_Lay_36.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_36.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_36.Controls.Add(Me.FlowLayoutPanel36, 0, 1)
        Me.Tbl_Lay_36.Controls.Add(Me.lbl_36, 0, 0)
        Me.Tbl_Lay_36.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_36.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_36.Name = "Tbl_Lay_36"
        Me.Tbl_Lay_36.RowCount = 2
        Me.Tbl_Lay_36.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_36.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_36.Size = New System.Drawing.Size(101, 87)
        Me.Tbl_Lay_36.TabIndex = 2
        Me.Tbl_Lay_36.Visible = False
        '
        'FlowLayoutPanel36
        '
        Me.FlowLayoutPanel36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel36.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel36.Location = New System.Drawing.Point(3, 46)
        Me.FlowLayoutPanel36.Name = "FlowLayoutPanel36"
        Me.FlowLayoutPanel36.Size = New System.Drawing.Size(95, 38)
        Me.FlowLayoutPanel36.TabIndex = 2
        '
        'lbl_36
        '
        Me.lbl_36.AutoSize = True
        Me.lbl_36.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_36.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_36.Location = New System.Drawing.Point(3, 0)
        Me.lbl_36.Name = "lbl_36"
        Me.lbl_36.Size = New System.Drawing.Size(95, 43)
        Me.lbl_36.TabIndex = 0
        Me.lbl_36.Text = "36"
        Me.lbl_36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel35
        '
        Me.Panel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel35.Controls.Add(Me.Tbl_Lay_35)
        Me.Panel35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel35.Location = New System.Drawing.Point(669, 375)
        Me.Panel35.Name = "Panel35"
        Me.Panel35.Size = New System.Drawing.Size(112, 87)
        Me.Panel35.TabIndex = 34
        '
        'Tbl_Lay_35
        '
        Me.Tbl_Lay_35.ColumnCount = 1
        Me.Tbl_Lay_35.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_35.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_35.Controls.Add(Me.FlowLayoutPanel34, 0, 1)
        Me.Tbl_Lay_35.Controls.Add(Me.lbl_35, 0, 0)
        Me.Tbl_Lay_35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_35.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_35.Name = "Tbl_Lay_35"
        Me.Tbl_Lay_35.RowCount = 2
        Me.Tbl_Lay_35.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_35.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_35.Size = New System.Drawing.Size(108, 83)
        Me.Tbl_Lay_35.TabIndex = 2
        Me.Tbl_Lay_35.Visible = False
        '
        'FlowLayoutPanel34
        '
        Me.FlowLayoutPanel34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel34.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel34.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel34.Name = "FlowLayoutPanel34"
        Me.FlowLayoutPanel34.Size = New System.Drawing.Size(102, 36)
        Me.FlowLayoutPanel34.TabIndex = 2
        '
        'lbl_35
        '
        Me.lbl_35.AutoSize = True
        Me.lbl_35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_35.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_35.Location = New System.Drawing.Point(3, 0)
        Me.lbl_35.Name = "lbl_35"
        Me.lbl_35.Size = New System.Drawing.Size(102, 41)
        Me.lbl_35.TabIndex = 0
        Me.lbl_35.Text = "35"
        Me.lbl_35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Tbl_Lay_1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(105, 87)
        Me.Panel1.TabIndex = 0
        '
        'Tbl_Lay_1
        '
        Me.Tbl_Lay_1.ColumnCount = 1
        Me.Tbl_Lay_1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_1.Controls.Add(Me.lbl_1, 0, 0)
        Me.Tbl_Lay_1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.Tbl_Lay_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_1.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_1.Name = "Tbl_Lay_1"
        Me.Tbl_Lay_1.RowCount = 2
        Me.Tbl_Lay_1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_1.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_1.TabIndex = 0
        Me.Tbl_Lay_1.Visible = False
        '
        'lbl_1
        '
        Me.lbl_1.AutoSize = True
        Me.lbl_1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_1.Location = New System.Drawing.Point(3, 0)
        Me.lbl_1.Name = "lbl_1"
        Me.lbl_1.Size = New System.Drawing.Size(95, 41)
        Me.lbl_1.TabIndex = 0
        Me.lbl_1.Text = "1"
        Me.lbl_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.Tbl_Lay_2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(114, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(105, 87)
        Me.Panel2.TabIndex = 1
        '
        'Tbl_Lay_2
        '
        Me.Tbl_Lay_2.ColumnCount = 1
        Me.Tbl_Lay_2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_2.Controls.Add(Me.lbl_2, 0, 0)
        Me.Tbl_Lay_2.Controls.Add(Me.FlowLayoutPanel2, 0, 1)
        Me.Tbl_Lay_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_2.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_2.Name = "Tbl_Lay_2"
        Me.Tbl_Lay_2.RowCount = 2
        Me.Tbl_Lay_2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_2.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_2.TabIndex = 0
        Me.Tbl_Lay_2.Visible = False
        '
        'lbl_2
        '
        Me.lbl_2.AutoSize = True
        Me.lbl_2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_2.Location = New System.Drawing.Point(3, 0)
        Me.lbl_2.Name = "lbl_2"
        Me.lbl_2.Size = New System.Drawing.Size(95, 41)
        Me.lbl_2.TabIndex = 0
        Me.lbl_2.Text = "2"
        Me.lbl_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.Tbl_Lay_3)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(225, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(105, 87)
        Me.Panel3.TabIndex = 2
        '
        'Tbl_Lay_3
        '
        Me.Tbl_Lay_3.ColumnCount = 1
        Me.Tbl_Lay_3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_3.Controls.Add(Me.lbl_3, 0, 0)
        Me.Tbl_Lay_3.Controls.Add(Me.FlowLayoutPanel3, 0, 1)
        Me.Tbl_Lay_3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_3.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_3.Name = "Tbl_Lay_3"
        Me.Tbl_Lay_3.RowCount = 2
        Me.Tbl_Lay_3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_3.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_3.TabIndex = 0
        Me.Tbl_Lay_3.Visible = False
        '
        'lbl_3
        '
        Me.lbl_3.AutoSize = True
        Me.lbl_3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_3.Location = New System.Drawing.Point(3, 0)
        Me.lbl_3.Name = "lbl_3"
        Me.lbl_3.Size = New System.Drawing.Size(95, 41)
        Me.lbl_3.TabIndex = 0
        Me.lbl_3.Text = "3"
        Me.lbl_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel3.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.Tbl_Lay_4)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(336, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(105, 87)
        Me.Panel4.TabIndex = 3
        '
        'Tbl_Lay_4
        '
        Me.Tbl_Lay_4.ColumnCount = 1
        Me.Tbl_Lay_4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_4.Controls.Add(Me.FlowLayoutPanel4, 0, 1)
        Me.Tbl_Lay_4.Controls.Add(Me.lbl_4, 0, 0)
        Me.Tbl_Lay_4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_4.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_4.Name = "Tbl_Lay_4"
        Me.Tbl_Lay_4.RowCount = 2
        Me.Tbl_Lay_4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_4.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_4.TabIndex = 0
        Me.Tbl_Lay_4.Visible = False
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'lbl_4
        '
        Me.lbl_4.AutoSize = True
        Me.lbl_4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_4.Location = New System.Drawing.Point(3, 0)
        Me.lbl_4.Name = "lbl_4"
        Me.lbl_4.Size = New System.Drawing.Size(95, 41)
        Me.lbl_4.TabIndex = 0
        Me.lbl_4.Text = "4"
        Me.lbl_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel5.Controls.Add(Me.Tbl_Lay_8)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 96)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(105, 87)
        Me.Panel5.TabIndex = 4
        '
        'Tbl_Lay_8
        '
        Me.Tbl_Lay_8.ColumnCount = 1
        Me.Tbl_Lay_8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_8.Controls.Add(Me.FlowLayoutPanel8, 0, 1)
        Me.Tbl_Lay_8.Controls.Add(Me.lbl_8, 0, 0)
        Me.Tbl_Lay_8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_8.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_8.Name = "Tbl_Lay_8"
        Me.Tbl_Lay_8.RowCount = 2
        Me.Tbl_Lay_8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_8.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_8.TabIndex = 0
        Me.Tbl_Lay_8.Visible = False
        '
        'FlowLayoutPanel8
        '
        Me.FlowLayoutPanel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel8.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
        Me.FlowLayoutPanel8.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel8.TabIndex = 2
        '
        'lbl_8
        '
        Me.lbl_8.AutoSize = True
        Me.lbl_8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_8.Location = New System.Drawing.Point(3, 0)
        Me.lbl_8.Name = "lbl_8"
        Me.lbl_8.Size = New System.Drawing.Size(95, 41)
        Me.lbl_8.TabIndex = 0
        Me.lbl_8.Text = "8"
        Me.lbl_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel6.Controls.Add(Me.Tbl_Lay_9)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(114, 96)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(105, 87)
        Me.Panel6.TabIndex = 5
        '
        'Tbl_Lay_9
        '
        Me.Tbl_Lay_9.ColumnCount = 1
        Me.Tbl_Lay_9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_9.Controls.Add(Me.FlowLayoutPanel9, 0, 1)
        Me.Tbl_Lay_9.Controls.Add(Me.lbl_9, 0, 0)
        Me.Tbl_Lay_9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_9.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_9.Name = "Tbl_Lay_9"
        Me.Tbl_Lay_9.RowCount = 2
        Me.Tbl_Lay_9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_9.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_9.TabIndex = 0
        Me.Tbl_Lay_9.Visible = False
        '
        'FlowLayoutPanel9
        '
        Me.FlowLayoutPanel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel9.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
        Me.FlowLayoutPanel9.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel9.TabIndex = 2
        '
        'lbl_9
        '
        Me.lbl_9.AutoSize = True
        Me.lbl_9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_9.Location = New System.Drawing.Point(3, 0)
        Me.lbl_9.Name = "lbl_9"
        Me.lbl_9.Size = New System.Drawing.Size(95, 41)
        Me.lbl_9.TabIndex = 0
        Me.lbl_9.Text = "9"
        Me.lbl_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel7.Controls.Add(Me.Tbl_Lay_10)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(225, 96)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(105, 87)
        Me.Panel7.TabIndex = 6
        '
        'Tbl_Lay_10
        '
        Me.Tbl_Lay_10.ColumnCount = 1
        Me.Tbl_Lay_10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_10.Controls.Add(Me.FlowLayoutPanel10, 0, 1)
        Me.Tbl_Lay_10.Controls.Add(Me.lbl_10, 0, 0)
        Me.Tbl_Lay_10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_10.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_10.Name = "Tbl_Lay_10"
        Me.Tbl_Lay_10.RowCount = 2
        Me.Tbl_Lay_10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_10.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_10.TabIndex = 0
        Me.Tbl_Lay_10.Visible = False
        '
        'FlowLayoutPanel10
        '
        Me.FlowLayoutPanel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel10.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
        Me.FlowLayoutPanel10.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel10.TabIndex = 2
        '
        'lbl_10
        '
        Me.lbl_10.AutoSize = True
        Me.lbl_10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_10.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_10.Location = New System.Drawing.Point(3, 0)
        Me.lbl_10.Name = "lbl_10"
        Me.lbl_10.Size = New System.Drawing.Size(95, 41)
        Me.lbl_10.TabIndex = 0
        Me.lbl_10.Text = "10"
        Me.lbl_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel8.Controls.Add(Me.Tbl_Lay_11)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel8.Location = New System.Drawing.Point(336, 96)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(105, 87)
        Me.Panel8.TabIndex = 7
        '
        'Tbl_Lay_11
        '
        Me.Tbl_Lay_11.ColumnCount = 1
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.Tbl_Lay_11.Controls.Add(Me.FlowLayoutPanel11, 0, 1)
        Me.Tbl_Lay_11.Controls.Add(Me.lbl_11, 0, 0)
        Me.Tbl_Lay_11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_11.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_11.Name = "Tbl_Lay_11"
        Me.Tbl_Lay_11.RowCount = 2
        Me.Tbl_Lay_11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_11.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_11.TabIndex = 0
        Me.Tbl_Lay_11.Visible = False
        '
        'FlowLayoutPanel11
        '
        Me.FlowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel11.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
        Me.FlowLayoutPanel11.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel11.TabIndex = 2
        '
        'lbl_11
        '
        Me.lbl_11.AutoSize = True
        Me.lbl_11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_11.Location = New System.Drawing.Point(3, 0)
        Me.lbl_11.Name = "lbl_11"
        Me.lbl_11.Size = New System.Drawing.Size(95, 41)
        Me.lbl_11.TabIndex = 0
        Me.lbl_11.Text = "11"
        Me.lbl_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel9
        '
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel9.Controls.Add(Me.Tbl_Lay_15)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(3, 189)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(105, 87)
        Me.Panel9.TabIndex = 8
        '
        'Tbl_Lay_15
        '
        Me.Tbl_Lay_15.ColumnCount = 1
        Me.Tbl_Lay_15.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_15.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_15.Controls.Add(Me.FlowLayoutPanel15, 0, 1)
        Me.Tbl_Lay_15.Controls.Add(Me.lbl_15, 0, 0)
        Me.Tbl_Lay_15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_15.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_15.Name = "Tbl_Lay_15"
        Me.Tbl_Lay_15.RowCount = 2
        Me.Tbl_Lay_15.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_15.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_15.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_15.TabIndex = 0
        Me.Tbl_Lay_15.Visible = False
        '
        'FlowLayoutPanel15
        '
        Me.FlowLayoutPanel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel15.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel15.Name = "FlowLayoutPanel15"
        Me.FlowLayoutPanel15.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel15.TabIndex = 2
        '
        'lbl_15
        '
        Me.lbl_15.AutoSize = True
        Me.lbl_15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_15.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_15.Location = New System.Drawing.Point(3, 0)
        Me.lbl_15.Name = "lbl_15"
        Me.lbl_15.Size = New System.Drawing.Size(95, 41)
        Me.lbl_15.TabIndex = 0
        Me.lbl_15.Text = "15"
        Me.lbl_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel10
        '
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel10.Controls.Add(Me.Tbl_Lay_16)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(114, 189)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(105, 87)
        Me.Panel10.TabIndex = 9
        '
        'Tbl_Lay_16
        '
        Me.Tbl_Lay_16.ColumnCount = 1
        Me.Tbl_Lay_16.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_16.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_16.Controls.Add(Me.FlowLayoutPanel16, 0, 1)
        Me.Tbl_Lay_16.Controls.Add(Me.lbl_16, 0, 0)
        Me.Tbl_Lay_16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_16.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tbl_Lay_16.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_16.Name = "Tbl_Lay_16"
        Me.Tbl_Lay_16.RowCount = 2
        Me.Tbl_Lay_16.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_16.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_16.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_16.TabIndex = 0
        Me.Tbl_Lay_16.Visible = False
        '
        'FlowLayoutPanel16
        '
        Me.FlowLayoutPanel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel16.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel16.Name = "FlowLayoutPanel16"
        Me.FlowLayoutPanel16.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel16.TabIndex = 2
        '
        'lbl_16
        '
        Me.lbl_16.AutoSize = True
        Me.lbl_16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_16.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_16.Location = New System.Drawing.Point(3, 0)
        Me.lbl_16.Name = "lbl_16"
        Me.lbl_16.Size = New System.Drawing.Size(95, 41)
        Me.lbl_16.TabIndex = 0
        Me.lbl_16.Text = "16"
        Me.lbl_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel11
        '
        Me.Panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel11.Controls.Add(Me.Tbl_Lay_17)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel11.Location = New System.Drawing.Point(225, 189)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(105, 87)
        Me.Panel11.TabIndex = 10
        '
        'Tbl_Lay_17
        '
        Me.Tbl_Lay_17.ColumnCount = 1
        Me.Tbl_Lay_17.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_17.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_17.Controls.Add(Me.FlowLayoutPanel17, 0, 1)
        Me.Tbl_Lay_17.Controls.Add(Me.lbl_17, 0, 0)
        Me.Tbl_Lay_17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_17.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_17.Name = "Tbl_Lay_17"
        Me.Tbl_Lay_17.RowCount = 2
        Me.Tbl_Lay_17.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_17.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_17.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_17.TabIndex = 0
        Me.Tbl_Lay_17.Visible = False
        '
        'FlowLayoutPanel17
        '
        Me.FlowLayoutPanel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel17.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel17.Name = "FlowLayoutPanel17"
        Me.FlowLayoutPanel17.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel17.TabIndex = 2
        '
        'lbl_17
        '
        Me.lbl_17.AutoSize = True
        Me.lbl_17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_17.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_17.Location = New System.Drawing.Point(3, 0)
        Me.lbl_17.Name = "lbl_17"
        Me.lbl_17.Size = New System.Drawing.Size(95, 41)
        Me.lbl_17.TabIndex = 0
        Me.lbl_17.Text = "17"
        Me.lbl_17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel12
        '
        Me.Panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel12.Controls.Add(Me.Tbl_Lay_18)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel12.Location = New System.Drawing.Point(336, 189)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(105, 87)
        Me.Panel12.TabIndex = 11
        '
        'Tbl_Lay_18
        '
        Me.Tbl_Lay_18.ColumnCount = 1
        Me.Tbl_Lay_18.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_18.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_18.Controls.Add(Me.FlowLayoutPanel18, 0, 1)
        Me.Tbl_Lay_18.Controls.Add(Me.lbl_18, 0, 0)
        Me.Tbl_Lay_18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_18.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_18.Name = "Tbl_Lay_18"
        Me.Tbl_Lay_18.RowCount = 2
        Me.Tbl_Lay_18.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_18.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_18.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_18.TabIndex = 0
        Me.Tbl_Lay_18.Visible = False
        '
        'FlowLayoutPanel18
        '
        Me.FlowLayoutPanel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel18.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel18.Name = "FlowLayoutPanel18"
        Me.FlowLayoutPanel18.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel18.TabIndex = 2
        '
        'lbl_18
        '
        Me.lbl_18.AutoSize = True
        Me.lbl_18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_18.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_18.Location = New System.Drawing.Point(3, 0)
        Me.lbl_18.Name = "lbl_18"
        Me.lbl_18.Size = New System.Drawing.Size(95, 41)
        Me.lbl_18.TabIndex = 0
        Me.lbl_18.Text = "18"
        Me.lbl_18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel13
        '
        Me.Panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel13.Controls.Add(Me.Tbl_Lay_5)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel13.Location = New System.Drawing.Point(447, 3)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(105, 87)
        Me.Panel13.TabIndex = 12
        '
        'Tbl_Lay_5
        '
        Me.Tbl_Lay_5.ColumnCount = 1
        Me.Tbl_Lay_5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_5.Controls.Add(Me.FlowLayoutPanel5, 0, 1)
        Me.Tbl_Lay_5.Controls.Add(Me.lbl_5, 0, 0)
        Me.Tbl_Lay_5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_5.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_5.Name = "Tbl_Lay_5"
        Me.Tbl_Lay_5.RowCount = 2
        Me.Tbl_Lay_5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_5.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_5.TabIndex = 0
        Me.Tbl_Lay_5.Visible = False
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel5.TabIndex = 2
        '
        'lbl_5
        '
        Me.lbl_5.AutoSize = True
        Me.lbl_5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_5.Location = New System.Drawing.Point(3, 0)
        Me.lbl_5.Name = "lbl_5"
        Me.lbl_5.Size = New System.Drawing.Size(95, 41)
        Me.lbl_5.TabIndex = 0
        Me.lbl_5.Text = "5"
        Me.lbl_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel14
        '
        Me.Panel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel14.Controls.Add(Me.Tbl_Lay_6)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel14.Location = New System.Drawing.Point(558, 3)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(105, 87)
        Me.Panel14.TabIndex = 13
        '
        'Tbl_Lay_6
        '
        Me.Tbl_Lay_6.ColumnCount = 1
        Me.Tbl_Lay_6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_6.Controls.Add(Me.FlowLayoutPanel6, 0, 1)
        Me.Tbl_Lay_6.Controls.Add(Me.lbl_6, 0, 0)
        Me.Tbl_Lay_6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_6.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_6.Name = "Tbl_Lay_6"
        Me.Tbl_Lay_6.RowCount = 2
        Me.Tbl_Lay_6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_6.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_6.TabIndex = 0
        Me.Tbl_Lay_6.Visible = False
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel6.TabIndex = 2
        '
        'lbl_6
        '
        Me.lbl_6.AutoSize = True
        Me.lbl_6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_6.Location = New System.Drawing.Point(3, 0)
        Me.lbl_6.Name = "lbl_6"
        Me.lbl_6.Size = New System.Drawing.Size(95, 41)
        Me.lbl_6.TabIndex = 0
        Me.lbl_6.Text = "6"
        Me.lbl_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel15
        '
        Me.Panel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel15.Controls.Add(Me.Tbl_Lay_7)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel15.Location = New System.Drawing.Point(669, 3)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(112, 87)
        Me.Panel15.TabIndex = 14
        '
        'Tbl_Lay_7
        '
        Me.Tbl_Lay_7.ColumnCount = 1
        Me.Tbl_Lay_7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_7.Controls.Add(Me.FlowLayoutPanel7, 0, 1)
        Me.Tbl_Lay_7.Controls.Add(Me.lbl_7, 0, 0)
        Me.Tbl_Lay_7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_7.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_7.Name = "Tbl_Lay_7"
        Me.Tbl_Lay_7.RowCount = 2
        Me.Tbl_Lay_7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_7.Size = New System.Drawing.Size(108, 83)
        Me.Tbl_Lay_7.TabIndex = 0
        Me.Tbl_Lay_7.Visible = False
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(102, 36)
        Me.FlowLayoutPanel7.TabIndex = 2
        '
        'lbl_7
        '
        Me.lbl_7.AutoSize = True
        Me.lbl_7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_7.Location = New System.Drawing.Point(3, 0)
        Me.lbl_7.Name = "lbl_7"
        Me.lbl_7.Size = New System.Drawing.Size(102, 41)
        Me.lbl_7.TabIndex = 0
        Me.lbl_7.Text = "7"
        Me.lbl_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel16
        '
        Me.Panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel16.Controls.Add(Me.Tbl_Lay_12)
        Me.Panel16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel16.Location = New System.Drawing.Point(447, 96)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(105, 87)
        Me.Panel16.TabIndex = 15
        '
        'Tbl_Lay_12
        '
        Me.Tbl_Lay_12.ColumnCount = 1
        Me.Tbl_Lay_12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_12.Controls.Add(Me.FlowLayoutPanel12, 0, 1)
        Me.Tbl_Lay_12.Controls.Add(Me.lbl_12, 0, 0)
        Me.Tbl_Lay_12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_12.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_12.Name = "Tbl_Lay_12"
        Me.Tbl_Lay_12.RowCount = 2
        Me.Tbl_Lay_12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_12.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_12.TabIndex = 0
        Me.Tbl_Lay_12.Visible = False
        '
        'FlowLayoutPanel12
        '
        Me.FlowLayoutPanel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel12.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel12.Name = "FlowLayoutPanel12"
        Me.FlowLayoutPanel12.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel12.TabIndex = 2
        '
        'lbl_12
        '
        Me.lbl_12.AutoSize = True
        Me.lbl_12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_12.Location = New System.Drawing.Point(3, 0)
        Me.lbl_12.Name = "lbl_12"
        Me.lbl_12.Size = New System.Drawing.Size(95, 41)
        Me.lbl_12.TabIndex = 0
        Me.lbl_12.Text = "12"
        Me.lbl_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel17
        '
        Me.Panel17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel17.Controls.Add(Me.Tbl_Lay_13)
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel17.Location = New System.Drawing.Point(558, 96)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(105, 87)
        Me.Panel17.TabIndex = 16
        '
        'Tbl_Lay_13
        '
        Me.Tbl_Lay_13.ColumnCount = 1
        Me.Tbl_Lay_13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_13.Controls.Add(Me.FlowLayoutPanel13, 0, 1)
        Me.Tbl_Lay_13.Controls.Add(Me.lbl_13, 0, 0)
        Me.Tbl_Lay_13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_13.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_13.Name = "Tbl_Lay_13"
        Me.Tbl_Lay_13.RowCount = 2
        Me.Tbl_Lay_13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_13.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_13.TabIndex = 0
        Me.Tbl_Lay_13.Visible = False
        '
        'FlowLayoutPanel13
        '
        Me.FlowLayoutPanel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel13.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel13.Name = "FlowLayoutPanel13"
        Me.FlowLayoutPanel13.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel13.TabIndex = 2
        '
        'lbl_13
        '
        Me.lbl_13.AutoSize = True
        Me.lbl_13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_13.Location = New System.Drawing.Point(3, 0)
        Me.lbl_13.Name = "lbl_13"
        Me.lbl_13.Size = New System.Drawing.Size(95, 41)
        Me.lbl_13.TabIndex = 0
        Me.lbl_13.Text = "13"
        Me.lbl_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel18
        '
        Me.Panel18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel18.Controls.Add(Me.Tbl_Lay_14)
        Me.Panel18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel18.Location = New System.Drawing.Point(669, 96)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(112, 87)
        Me.Panel18.TabIndex = 17
        '
        'Tbl_Lay_14
        '
        Me.Tbl_Lay_14.ColumnCount = 1
        Me.Tbl_Lay_14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_14.Controls.Add(Me.FlowLayoutPanel14, 0, 1)
        Me.Tbl_Lay_14.Controls.Add(Me.lbl_14, 0, 0)
        Me.Tbl_Lay_14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_14.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_14.Name = "Tbl_Lay_14"
        Me.Tbl_Lay_14.RowCount = 2
        Me.Tbl_Lay_14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_14.Size = New System.Drawing.Size(108, 83)
        Me.Tbl_Lay_14.TabIndex = 0
        Me.Tbl_Lay_14.Visible = False
        '
        'FlowLayoutPanel14
        '
        Me.FlowLayoutPanel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel14.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel14.Name = "FlowLayoutPanel14"
        Me.FlowLayoutPanel14.Size = New System.Drawing.Size(102, 36)
        Me.FlowLayoutPanel14.TabIndex = 2
        '
        'lbl_14
        '
        Me.lbl_14.AutoSize = True
        Me.lbl_14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_14.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_14.Location = New System.Drawing.Point(3, 0)
        Me.lbl_14.Name = "lbl_14"
        Me.lbl_14.Size = New System.Drawing.Size(102, 41)
        Me.lbl_14.TabIndex = 0
        Me.lbl_14.Text = "14"
        Me.lbl_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel19
        '
        Me.Panel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel19.Controls.Add(Me.Tbl_Lay_19)
        Me.Panel19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel19.Location = New System.Drawing.Point(447, 189)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(105, 87)
        Me.Panel19.TabIndex = 18
        '
        'Tbl_Lay_19
        '
        Me.Tbl_Lay_19.ColumnCount = 1
        Me.Tbl_Lay_19.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_19.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_19.Controls.Add(Me.FlowLayoutPanel19, 0, 1)
        Me.Tbl_Lay_19.Controls.Add(Me.lbl_19, 0, 0)
        Me.Tbl_Lay_19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_19.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_19.Name = "Tbl_Lay_19"
        Me.Tbl_Lay_19.RowCount = 2
        Me.Tbl_Lay_19.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_19.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_19.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_19.TabIndex = 0
        Me.Tbl_Lay_19.Visible = False
        '
        'FlowLayoutPanel19
        '
        Me.FlowLayoutPanel19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel19.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel19.Name = "FlowLayoutPanel19"
        Me.FlowLayoutPanel19.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel19.TabIndex = 2
        '
        'lbl_19
        '
        Me.lbl_19.AutoSize = True
        Me.lbl_19.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_19.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_19.Location = New System.Drawing.Point(3, 0)
        Me.lbl_19.Name = "lbl_19"
        Me.lbl_19.Size = New System.Drawing.Size(95, 41)
        Me.lbl_19.TabIndex = 0
        Me.lbl_19.Text = "19"
        Me.lbl_19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel20
        '
        Me.Panel20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel20.Controls.Add(Me.Tbl_Lay_20)
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel20.Location = New System.Drawing.Point(558, 189)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(105, 87)
        Me.Panel20.TabIndex = 19
        '
        'Tbl_Lay_20
        '
        Me.Tbl_Lay_20.ColumnCount = 1
        Me.Tbl_Lay_20.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_20.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_20.Controls.Add(Me.FlowLayoutPanel20, 0, 1)
        Me.Tbl_Lay_20.Controls.Add(Me.lbl_20, 0, 0)
        Me.Tbl_Lay_20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_20.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_20.Name = "Tbl_Lay_20"
        Me.Tbl_Lay_20.RowCount = 2
        Me.Tbl_Lay_20.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_20.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_20.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_20.TabIndex = 0
        Me.Tbl_Lay_20.Visible = False
        '
        'FlowLayoutPanel20
        '
        Me.FlowLayoutPanel20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel20.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel20.Name = "FlowLayoutPanel20"
        Me.FlowLayoutPanel20.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel20.TabIndex = 2
        '
        'lbl_20
        '
        Me.lbl_20.AutoSize = True
        Me.lbl_20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_20.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_20.Location = New System.Drawing.Point(3, 0)
        Me.lbl_20.Name = "lbl_20"
        Me.lbl_20.Size = New System.Drawing.Size(95, 41)
        Me.lbl_20.TabIndex = 0
        Me.lbl_20.Text = "20"
        Me.lbl_20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel21
        '
        Me.Panel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel21.Controls.Add(Me.Tbl_Lay_21)
        Me.Panel21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel21.Location = New System.Drawing.Point(669, 189)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(112, 87)
        Me.Panel21.TabIndex = 20
        '
        'Tbl_Lay_21
        '
        Me.Tbl_Lay_21.ColumnCount = 1
        Me.Tbl_Lay_21.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_21.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_21.Controls.Add(Me.FlowLayoutPanel21, 0, 1)
        Me.Tbl_Lay_21.Controls.Add(Me.lbl_21, 0, 0)
        Me.Tbl_Lay_21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_21.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_21.Name = "Tbl_Lay_21"
        Me.Tbl_Lay_21.RowCount = 2
        Me.Tbl_Lay_21.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_21.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_21.Size = New System.Drawing.Size(108, 83)
        Me.Tbl_Lay_21.TabIndex = 0
        Me.Tbl_Lay_21.Visible = False
        '
        'FlowLayoutPanel21
        '
        Me.FlowLayoutPanel21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel21.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel21.Name = "FlowLayoutPanel21"
        Me.FlowLayoutPanel21.Size = New System.Drawing.Size(102, 36)
        Me.FlowLayoutPanel21.TabIndex = 2
        '
        'lbl_21
        '
        Me.lbl_21.AutoSize = True
        Me.lbl_21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_21.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_21.Location = New System.Drawing.Point(3, 0)
        Me.lbl_21.Name = "lbl_21"
        Me.lbl_21.Size = New System.Drawing.Size(102, 41)
        Me.lbl_21.TabIndex = 0
        Me.lbl_21.Text = "21"
        Me.lbl_21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel22
        '
        Me.Panel22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel22.Controls.Add(Me.Tbl_Lay_22)
        Me.Panel22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel22.Location = New System.Drawing.Point(3, 282)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(105, 87)
        Me.Panel22.TabIndex = 21
        '
        'Tbl_Lay_22
        '
        Me.Tbl_Lay_22.ColumnCount = 1
        Me.Tbl_Lay_22.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_22.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_22.Controls.Add(Me.FlowLayoutPanel22, 0, 1)
        Me.Tbl_Lay_22.Controls.Add(Me.lbl_22, 0, 0)
        Me.Tbl_Lay_22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_22.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_22.Name = "Tbl_Lay_22"
        Me.Tbl_Lay_22.RowCount = 2
        Me.Tbl_Lay_22.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_22.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_22.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_22.TabIndex = 0
        Me.Tbl_Lay_22.Visible = False
        '
        'FlowLayoutPanel22
        '
        Me.FlowLayoutPanel22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel22.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel22.Name = "FlowLayoutPanel22"
        Me.FlowLayoutPanel22.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel22.TabIndex = 2
        '
        'lbl_22
        '
        Me.lbl_22.AutoSize = True
        Me.lbl_22.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_22.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_22.Location = New System.Drawing.Point(3, 0)
        Me.lbl_22.Name = "lbl_22"
        Me.lbl_22.Size = New System.Drawing.Size(95, 41)
        Me.lbl_22.TabIndex = 0
        Me.lbl_22.Text = "22"
        Me.lbl_22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel23
        '
        Me.Panel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel23.Controls.Add(Me.Tbl_Lay_23)
        Me.Panel23.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel23.Location = New System.Drawing.Point(114, 282)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(105, 87)
        Me.Panel23.TabIndex = 22
        '
        'Tbl_Lay_23
        '
        Me.Tbl_Lay_23.ColumnCount = 1
        Me.Tbl_Lay_23.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_23.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_23.Controls.Add(Me.FlowLayoutPanel23, 0, 1)
        Me.Tbl_Lay_23.Controls.Add(Me.lbl_23)
        Me.Tbl_Lay_23.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_23.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_23.Name = "Tbl_Lay_23"
        Me.Tbl_Lay_23.RowCount = 2
        Me.Tbl_Lay_23.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_23.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_23.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_23.TabIndex = 0
        Me.Tbl_Lay_23.Visible = False
        '
        'FlowLayoutPanel23
        '
        Me.FlowLayoutPanel23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel23.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel23.Name = "FlowLayoutPanel23"
        Me.FlowLayoutPanel23.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel23.TabIndex = 2
        '
        'lbl_23
        '
        Me.lbl_23.AutoSize = True
        Me.lbl_23.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_23.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_23.Location = New System.Drawing.Point(3, 0)
        Me.lbl_23.Name = "lbl_23"
        Me.lbl_23.Size = New System.Drawing.Size(95, 41)
        Me.lbl_23.TabIndex = 0
        Me.lbl_23.Text = "23"
        Me.lbl_23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel24
        '
        Me.Panel24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel24.Controls.Add(Me.Tbl_Lay_24)
        Me.Panel24.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel24.Location = New System.Drawing.Point(225, 282)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(105, 87)
        Me.Panel24.TabIndex = 23
        '
        'Tbl_Lay_24
        '
        Me.Tbl_Lay_24.ColumnCount = 1
        Me.Tbl_Lay_24.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_24.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_24.Controls.Add(Me.FlowLayoutPanel24, 0, 1)
        Me.Tbl_Lay_24.Controls.Add(Me.lbl_24, 0, 0)
        Me.Tbl_Lay_24.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_24.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_24.Name = "Tbl_Lay_24"
        Me.Tbl_Lay_24.RowCount = 2
        Me.Tbl_Lay_24.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_24.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_24.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_24.TabIndex = 0
        Me.Tbl_Lay_24.Visible = False
        '
        'FlowLayoutPanel24
        '
        Me.FlowLayoutPanel24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel24.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel24.Name = "FlowLayoutPanel24"
        Me.FlowLayoutPanel24.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel24.TabIndex = 2
        '
        'lbl_24
        '
        Me.lbl_24.AutoSize = True
        Me.lbl_24.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_24.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_24.Location = New System.Drawing.Point(3, 0)
        Me.lbl_24.Name = "lbl_24"
        Me.lbl_24.Size = New System.Drawing.Size(95, 41)
        Me.lbl_24.TabIndex = 0
        Me.lbl_24.Text = "24"
        Me.lbl_24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel25
        '
        Me.Panel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel25.Controls.Add(Me.Tbl_Lay_25)
        Me.Panel25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel25.Location = New System.Drawing.Point(336, 282)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(105, 87)
        Me.Panel25.TabIndex = 24
        '
        'Tbl_Lay_25
        '
        Me.Tbl_Lay_25.ColumnCount = 1
        Me.Tbl_Lay_25.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_25.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_25.Controls.Add(Me.FlowLayoutPanel25, 0, 1)
        Me.Tbl_Lay_25.Controls.Add(Me.lbl_25, 0, 0)
        Me.Tbl_Lay_25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_25.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_25.Name = "Tbl_Lay_25"
        Me.Tbl_Lay_25.RowCount = 2
        Me.Tbl_Lay_25.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_25.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_25.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_25.TabIndex = 0
        Me.Tbl_Lay_25.Visible = False
        '
        'FlowLayoutPanel25
        '
        Me.FlowLayoutPanel25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel25.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel25.Name = "FlowLayoutPanel25"
        Me.FlowLayoutPanel25.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel25.TabIndex = 2
        '
        'lbl_25
        '
        Me.lbl_25.AutoSize = True
        Me.lbl_25.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_25.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_25.Location = New System.Drawing.Point(3, 0)
        Me.lbl_25.Name = "lbl_25"
        Me.lbl_25.Size = New System.Drawing.Size(95, 41)
        Me.lbl_25.TabIndex = 0
        Me.lbl_25.Text = "25"
        Me.lbl_25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel26
        '
        Me.Panel26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel26.Controls.Add(Me.Tbl_Lay_26)
        Me.Panel26.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel26.Location = New System.Drawing.Point(447, 282)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(105, 87)
        Me.Panel26.TabIndex = 25
        '
        'Tbl_Lay_26
        '
        Me.Tbl_Lay_26.ColumnCount = 1
        Me.Tbl_Lay_26.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_26.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_26.Controls.Add(Me.FlowLayoutPanel26, 0, 1)
        Me.Tbl_Lay_26.Controls.Add(Me.lbl_26, 0, 0)
        Me.Tbl_Lay_26.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_26.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_26.Name = "Tbl_Lay_26"
        Me.Tbl_Lay_26.RowCount = 2
        Me.Tbl_Lay_26.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_26.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_26.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_26.TabIndex = 0
        Me.Tbl_Lay_26.Visible = False
        '
        'FlowLayoutPanel26
        '
        Me.FlowLayoutPanel26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel26.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel26.Name = "FlowLayoutPanel26"
        Me.FlowLayoutPanel26.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel26.TabIndex = 2
        '
        'lbl_26
        '
        Me.lbl_26.AutoSize = True
        Me.lbl_26.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_26.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_26.Location = New System.Drawing.Point(3, 0)
        Me.lbl_26.Name = "lbl_26"
        Me.lbl_26.Size = New System.Drawing.Size(95, 41)
        Me.lbl_26.TabIndex = 0
        Me.lbl_26.Text = "26"
        Me.lbl_26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel27
        '
        Me.Panel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel27.Controls.Add(Me.Tbl_Lay_27)
        Me.Panel27.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel27.Location = New System.Drawing.Point(558, 282)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Size = New System.Drawing.Size(105, 87)
        Me.Panel27.TabIndex = 26
        '
        'Tbl_Lay_27
        '
        Me.Tbl_Lay_27.ColumnCount = 1
        Me.Tbl_Lay_27.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_27.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_27.Controls.Add(Me.FlowLayoutPanel27, 0, 1)
        Me.Tbl_Lay_27.Controls.Add(Me.lbl_27, 0, 0)
        Me.Tbl_Lay_27.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_27.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_27.Name = "Tbl_Lay_27"
        Me.Tbl_Lay_27.RowCount = 2
        Me.Tbl_Lay_27.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_27.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_27.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_27.TabIndex = 0
        Me.Tbl_Lay_27.Visible = False
        '
        'FlowLayoutPanel27
        '
        Me.FlowLayoutPanel27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel27.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel27.Name = "FlowLayoutPanel27"
        Me.FlowLayoutPanel27.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel27.TabIndex = 2
        '
        'lbl_27
        '
        Me.lbl_27.AutoSize = True
        Me.lbl_27.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_27.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_27.Location = New System.Drawing.Point(3, 0)
        Me.lbl_27.Name = "lbl_27"
        Me.lbl_27.Size = New System.Drawing.Size(95, 41)
        Me.lbl_27.TabIndex = 0
        Me.lbl_27.Text = "27"
        Me.lbl_27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel28
        '
        Me.Panel28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel28.Controls.Add(Me.Tbl_Lay_28)
        Me.Panel28.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel28.Location = New System.Drawing.Point(669, 282)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Size = New System.Drawing.Size(112, 87)
        Me.Panel28.TabIndex = 27
        '
        'Tbl_Lay_28
        '
        Me.Tbl_Lay_28.ColumnCount = 1
        Me.Tbl_Lay_28.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_28.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_28.Controls.Add(Me.FlowLayoutPanel28, 0, 1)
        Me.Tbl_Lay_28.Controls.Add(Me.lbl_28, 0, 0)
        Me.Tbl_Lay_28.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_28.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_28.Name = "Tbl_Lay_28"
        Me.Tbl_Lay_28.RowCount = 2
        Me.Tbl_Lay_28.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_28.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_28.Size = New System.Drawing.Size(108, 83)
        Me.Tbl_Lay_28.TabIndex = 0
        Me.Tbl_Lay_28.Visible = False
        '
        'FlowLayoutPanel28
        '
        Me.FlowLayoutPanel28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel28.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel28.Name = "FlowLayoutPanel28"
        Me.FlowLayoutPanel28.Size = New System.Drawing.Size(102, 36)
        Me.FlowLayoutPanel28.TabIndex = 2
        '
        'lbl_28
        '
        Me.lbl_28.AutoSize = True
        Me.lbl_28.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_28.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_28.Location = New System.Drawing.Point(3, 0)
        Me.lbl_28.Name = "lbl_28"
        Me.lbl_28.Size = New System.Drawing.Size(102, 41)
        Me.lbl_28.TabIndex = 0
        Me.lbl_28.Text = "28"
        Me.lbl_28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel29
        '
        Me.Panel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel29.Controls.Add(Me.Tbl_Lay_29)
        Me.Panel29.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel29.Location = New System.Drawing.Point(3, 375)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Size = New System.Drawing.Size(105, 87)
        Me.Panel29.TabIndex = 28
        '
        'Tbl_Lay_29
        '
        Me.Tbl_Lay_29.ColumnCount = 1
        Me.Tbl_Lay_29.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_29.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_29.Controls.Add(Me.FlowLayoutPanel29, 0, 1)
        Me.Tbl_Lay_29.Controls.Add(Me.lbl_29, 0, 0)
        Me.Tbl_Lay_29.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_29.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_29.Name = "Tbl_Lay_29"
        Me.Tbl_Lay_29.RowCount = 2
        Me.Tbl_Lay_29.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_29.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_29.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_29.TabIndex = 0
        Me.Tbl_Lay_29.Visible = False
        '
        'FlowLayoutPanel29
        '
        Me.FlowLayoutPanel29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel29.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel29.Name = "FlowLayoutPanel29"
        Me.FlowLayoutPanel29.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel29.TabIndex = 2
        '
        'lbl_29
        '
        Me.lbl_29.AutoSize = True
        Me.lbl_29.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_29.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_29.Location = New System.Drawing.Point(3, 0)
        Me.lbl_29.Name = "lbl_29"
        Me.lbl_29.Size = New System.Drawing.Size(95, 41)
        Me.lbl_29.TabIndex = 0
        Me.lbl_29.Text = "29"
        Me.lbl_29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel30
        '
        Me.Panel30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel30.Controls.Add(Me.Tbl_Lay_30)
        Me.Panel30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel30.Location = New System.Drawing.Point(114, 375)
        Me.Panel30.Name = "Panel30"
        Me.Panel30.Size = New System.Drawing.Size(105, 87)
        Me.Panel30.TabIndex = 29
        '
        'Tbl_Lay_30
        '
        Me.Tbl_Lay_30.ColumnCount = 1
        Me.Tbl_Lay_30.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_30.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_30.Controls.Add(Me.FlowLayoutPanel30, 0, 1)
        Me.Tbl_Lay_30.Controls.Add(Me.lbl_30, 0, 0)
        Me.Tbl_Lay_30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_30.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_30.Name = "Tbl_Lay_30"
        Me.Tbl_Lay_30.RowCount = 2
        Me.Tbl_Lay_30.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_30.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_30.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_30.TabIndex = 0
        Me.Tbl_Lay_30.Visible = False
        '
        'FlowLayoutPanel30
        '
        Me.FlowLayoutPanel30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel30.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel30.Name = "FlowLayoutPanel30"
        Me.FlowLayoutPanel30.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel30.TabIndex = 2
        '
        'lbl_30
        '
        Me.lbl_30.AutoSize = True
        Me.lbl_30.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_30.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_30.Location = New System.Drawing.Point(3, 0)
        Me.lbl_30.Name = "lbl_30"
        Me.lbl_30.Size = New System.Drawing.Size(95, 41)
        Me.lbl_30.TabIndex = 0
        Me.lbl_30.Text = "30"
        Me.lbl_30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel31
        '
        Me.Panel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel31.Controls.Add(Me.Tbl_Lay_31)
        Me.Panel31.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel31.Location = New System.Drawing.Point(225, 375)
        Me.Panel31.Name = "Panel31"
        Me.Panel31.Size = New System.Drawing.Size(105, 87)
        Me.Panel31.TabIndex = 30
        '
        'Tbl_Lay_31
        '
        Me.Tbl_Lay_31.ColumnCount = 1
        Me.Tbl_Lay_31.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_31.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_31.Controls.Add(Me.FlowLayoutPanel35, 0, 1)
        Me.Tbl_Lay_31.Controls.Add(Me.lbl_31, 0, 0)
        Me.Tbl_Lay_31.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_31.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_31.Name = "Tbl_Lay_31"
        Me.Tbl_Lay_31.RowCount = 2
        Me.Tbl_Lay_31.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_31.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_31.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_31.TabIndex = 1
        Me.Tbl_Lay_31.Visible = False
        '
        'FlowLayoutPanel35
        '
        Me.FlowLayoutPanel35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel35.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel35.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel35.Name = "FlowLayoutPanel35"
        Me.FlowLayoutPanel35.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel35.TabIndex = 3
        '
        'lbl_31
        '
        Me.lbl_31.AutoSize = True
        Me.lbl_31.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_31.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_31.Location = New System.Drawing.Point(3, 0)
        Me.lbl_31.Name = "lbl_31"
        Me.lbl_31.Size = New System.Drawing.Size(95, 41)
        Me.lbl_31.TabIndex = 0
        Me.lbl_31.Text = "31"
        Me.lbl_31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel32
        '
        Me.Panel32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel32.Controls.Add(Me.Tbl_Lay_32)
        Me.Panel32.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel32.Location = New System.Drawing.Point(336, 375)
        Me.Panel32.Name = "Panel32"
        Me.Panel32.Size = New System.Drawing.Size(105, 87)
        Me.Panel32.TabIndex = 31
        '
        'Tbl_Lay_32
        '
        Me.Tbl_Lay_32.ColumnCount = 1
        Me.Tbl_Lay_32.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_32.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_32.Controls.Add(Me.FlowLayoutPanel31, 0, 1)
        Me.Tbl_Lay_32.Controls.Add(Me.lbl_32, 0, 0)
        Me.Tbl_Lay_32.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_32.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_32.Name = "Tbl_Lay_32"
        Me.Tbl_Lay_32.RowCount = 2
        Me.Tbl_Lay_32.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_32.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_32.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_32.TabIndex = 1
        Me.Tbl_Lay_32.Visible = False
        '
        'FlowLayoutPanel31
        '
        Me.FlowLayoutPanel31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel31.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel31.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel31.Name = "FlowLayoutPanel31"
        Me.FlowLayoutPanel31.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel31.TabIndex = 2
        '
        'lbl_32
        '
        Me.lbl_32.AutoSize = True
        Me.lbl_32.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_32.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_32.Location = New System.Drawing.Point(3, 0)
        Me.lbl_32.Name = "lbl_32"
        Me.lbl_32.Size = New System.Drawing.Size(95, 41)
        Me.lbl_32.TabIndex = 0
        Me.lbl_32.Text = "32"
        Me.lbl_32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel33
        '
        Me.Panel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel33.Controls.Add(Me.Tbl_Lay_33)
        Me.Panel33.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel33.Location = New System.Drawing.Point(447, 375)
        Me.Panel33.Name = "Panel33"
        Me.Panel33.Size = New System.Drawing.Size(105, 87)
        Me.Panel33.TabIndex = 32
        '
        'Tbl_Lay_33
        '
        Me.Tbl_Lay_33.ColumnCount = 1
        Me.Tbl_Lay_33.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_33.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_33.Controls.Add(Me.FlowLayoutPanel32, 0, 1)
        Me.Tbl_Lay_33.Controls.Add(Me.lbl_33, 0, 0)
        Me.Tbl_Lay_33.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_33.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_33.Name = "Tbl_Lay_33"
        Me.Tbl_Lay_33.RowCount = 2
        Me.Tbl_Lay_33.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_33.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_33.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_33.TabIndex = 1
        Me.Tbl_Lay_33.Visible = False
        '
        'FlowLayoutPanel32
        '
        Me.FlowLayoutPanel32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel32.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel32.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel32.Name = "FlowLayoutPanel32"
        Me.FlowLayoutPanel32.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel32.TabIndex = 2
        '
        'lbl_33
        '
        Me.lbl_33.AutoSize = True
        Me.lbl_33.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_33.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_33.Location = New System.Drawing.Point(3, 0)
        Me.lbl_33.Name = "lbl_33"
        Me.lbl_33.Size = New System.Drawing.Size(95, 41)
        Me.lbl_33.TabIndex = 0
        Me.lbl_33.Text = "33"
        Me.lbl_33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel34
        '
        Me.Panel34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel34.Controls.Add(Me.Tbl_Lay_34)
        Me.Panel34.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel34.Location = New System.Drawing.Point(558, 375)
        Me.Panel34.Name = "Panel34"
        Me.Panel34.Size = New System.Drawing.Size(105, 87)
        Me.Panel34.TabIndex = 33
        '
        'Tbl_Lay_34
        '
        Me.Tbl_Lay_34.ColumnCount = 1
        Me.Tbl_Lay_34.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_34.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_34.Controls.Add(Me.FlowLayoutPanel33, 0, 1)
        Me.Tbl_Lay_34.Controls.Add(Me.lbl_34, 0, 0)
        Me.Tbl_Lay_34.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Tbl_Lay_34.Location = New System.Drawing.Point(0, 0)
        Me.Tbl_Lay_34.Name = "Tbl_Lay_34"
        Me.Tbl_Lay_34.RowCount = 2
        Me.Tbl_Lay_34.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_34.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Tbl_Lay_34.Size = New System.Drawing.Size(101, 83)
        Me.Tbl_Lay_34.TabIndex = 2
        Me.Tbl_Lay_34.Visible = False
        '
        'FlowLayoutPanel33
        '
        Me.FlowLayoutPanel33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FlowLayoutPanel33.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel33.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel33.Name = "FlowLayoutPanel33"
        Me.FlowLayoutPanel33.Size = New System.Drawing.Size(95, 36)
        Me.FlowLayoutPanel33.TabIndex = 2
        '
        'lbl_34
        '
        Me.lbl_34.AutoSize = True
        Me.lbl_34.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_34.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_34.Location = New System.Drawing.Point(3, 0)
        Me.lbl_34.Name = "lbl_34"
        Me.lbl_34.Size = New System.Drawing.Size(95, 41)
        Me.lbl_34.TabIndex = 0
        Me.lbl_34.Text = "34"
        Me.lbl_34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Calendario_Mensual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.Tbl_Lay)
        Me.Name = "Calendario_Mensual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calendario Mensual"
        Me.Tbl_Lay.ResumeLayout(False)
        Me.Panel42.ResumeLayout(False)
        Me.Tbl_Lay_42.ResumeLayout(False)
        Me.Tbl_Lay_42.PerformLayout()
        Me.Panel41.ResumeLayout(False)
        Me.Tbl_Lay_41.ResumeLayout(False)
        Me.Tbl_Lay_41.PerformLayout()
        Me.Panel40.ResumeLayout(False)
        Me.Tbl_Lay_40.ResumeLayout(False)
        Me.Tbl_Lay_40.PerformLayout()
        Me.Panel39.ResumeLayout(False)
        Me.Tbl_Lay_39.ResumeLayout(False)
        Me.Tbl_Lay_39.PerformLayout()
        Me.Panel38.ResumeLayout(False)
        Me.Tbl_Lay_38.ResumeLayout(False)
        Me.Tbl_Lay_38.PerformLayout()
        Me.Panel37.ResumeLayout(False)
        Me.Tbl_Lay_37.ResumeLayout(False)
        Me.Tbl_Lay_37.PerformLayout()
        Me.Panel36.ResumeLayout(False)
        Me.Tbl_Lay_36.ResumeLayout(False)
        Me.Tbl_Lay_36.PerformLayout()
        Me.Panel35.ResumeLayout(False)
        Me.Tbl_Lay_35.ResumeLayout(False)
        Me.Tbl_Lay_35.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Tbl_Lay_1.ResumeLayout(False)
        Me.Tbl_Lay_1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Tbl_Lay_2.ResumeLayout(False)
        Me.Tbl_Lay_2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Tbl_Lay_3.ResumeLayout(False)
        Me.Tbl_Lay_3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Tbl_Lay_4.ResumeLayout(False)
        Me.Tbl_Lay_4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Tbl_Lay_8.ResumeLayout(False)
        Me.Tbl_Lay_8.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Tbl_Lay_9.ResumeLayout(False)
        Me.Tbl_Lay_9.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Tbl_Lay_10.ResumeLayout(False)
        Me.Tbl_Lay_10.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Tbl_Lay_11.ResumeLayout(False)
        Me.Tbl_Lay_11.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Tbl_Lay_15.ResumeLayout(False)
        Me.Tbl_Lay_15.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Tbl_Lay_16.ResumeLayout(False)
        Me.Tbl_Lay_16.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Tbl_Lay_17.ResumeLayout(False)
        Me.Tbl_Lay_17.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Tbl_Lay_18.ResumeLayout(False)
        Me.Tbl_Lay_18.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Tbl_Lay_5.ResumeLayout(False)
        Me.Tbl_Lay_5.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Tbl_Lay_6.ResumeLayout(False)
        Me.Tbl_Lay_6.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Tbl_Lay_7.ResumeLayout(False)
        Me.Tbl_Lay_7.PerformLayout()
        Me.Panel16.ResumeLayout(False)
        Me.Tbl_Lay_12.ResumeLayout(False)
        Me.Tbl_Lay_12.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Tbl_Lay_13.ResumeLayout(False)
        Me.Tbl_Lay_13.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Tbl_Lay_14.ResumeLayout(False)
        Me.Tbl_Lay_14.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Tbl_Lay_19.ResumeLayout(False)
        Me.Tbl_Lay_19.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Tbl_Lay_20.ResumeLayout(False)
        Me.Tbl_Lay_20.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Tbl_Lay_21.ResumeLayout(False)
        Me.Tbl_Lay_21.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Tbl_Lay_22.ResumeLayout(False)
        Me.Tbl_Lay_22.PerformLayout()
        Me.Panel23.ResumeLayout(False)
        Me.Tbl_Lay_23.ResumeLayout(False)
        Me.Tbl_Lay_23.PerformLayout()
        Me.Panel24.ResumeLayout(False)
        Me.Tbl_Lay_24.ResumeLayout(False)
        Me.Tbl_Lay_24.PerformLayout()
        Me.Panel25.ResumeLayout(False)
        Me.Tbl_Lay_25.ResumeLayout(False)
        Me.Tbl_Lay_25.PerformLayout()
        Me.Panel26.ResumeLayout(False)
        Me.Tbl_Lay_26.ResumeLayout(False)
        Me.Tbl_Lay_26.PerformLayout()
        Me.Panel27.ResumeLayout(False)
        Me.Tbl_Lay_27.ResumeLayout(False)
        Me.Tbl_Lay_27.PerformLayout()
        Me.Panel28.ResumeLayout(False)
        Me.Tbl_Lay_28.ResumeLayout(False)
        Me.Tbl_Lay_28.PerformLayout()
        Me.Panel29.ResumeLayout(False)
        Me.Tbl_Lay_29.ResumeLayout(False)
        Me.Tbl_Lay_29.PerformLayout()
        Me.Panel30.ResumeLayout(False)
        Me.Tbl_Lay_30.ResumeLayout(False)
        Me.Tbl_Lay_30.PerformLayout()
        Me.Panel31.ResumeLayout(False)
        Me.Tbl_Lay_31.ResumeLayout(False)
        Me.Tbl_Lay_31.PerformLayout()
        Me.Panel32.ResumeLayout(False)
        Me.Tbl_Lay_32.ResumeLayout(False)
        Me.Tbl_Lay_32.PerformLayout()
        Me.Panel33.ResumeLayout(False)
        Me.Tbl_Lay_33.ResumeLayout(False)
        Me.Tbl_Lay_33.PerformLayout()
        Me.Panel34.ResumeLayout(False)
        Me.Tbl_Lay_34.ResumeLayout(False)
        Me.Tbl_Lay_34.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tbl_Lay As TableLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Tbl_Lay_2 As TableLayoutPanel
    Friend WithEvents lbl_2 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Tbl_Lay_3 As TableLayoutPanel
    Friend WithEvents lbl_3 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Tbl_Lay_4 As TableLayoutPanel
    Friend WithEvents lbl_4 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Tbl_Lay_8 As TableLayoutPanel
    Friend WithEvents lbl_8 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Tbl_Lay_9 As TableLayoutPanel
    Friend WithEvents lbl_9 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Tbl_Lay_10 As TableLayoutPanel
    Friend WithEvents lbl_10 As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Tbl_Lay_11 As TableLayoutPanel
    Friend WithEvents lbl_11 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Tbl_Lay_15 As TableLayoutPanel
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Tbl_Lay_16 As TableLayoutPanel
    Friend WithEvents lbl_16 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Tbl_Lay_17 As TableLayoutPanel
    Friend WithEvents lbl_17 As Label
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Tbl_Lay_18 As TableLayoutPanel
    Friend WithEvents lbl_18 As Label
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Panel14 As Panel
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Panel19 As Panel
    Friend WithEvents Panel20 As Panel
    Friend WithEvents Panel21 As Panel
    Friend WithEvents Panel22 As Panel
    Friend WithEvents Panel23 As Panel
    Friend WithEvents Panel24 As Panel
    Friend WithEvents Panel25 As Panel
    Friend WithEvents Panel26 As Panel
    Friend WithEvents Panel27 As Panel
    Friend WithEvents Panel28 As Panel
    Friend WithEvents Panel29 As Panel
    Friend WithEvents Panel30 As Panel
    Friend WithEvents Panel31 As Panel
    Friend WithEvents Tbl_Lay_5 As TableLayoutPanel
    Friend WithEvents lbl_5 As Label
    Friend WithEvents Tbl_Lay_6 As TableLayoutPanel
    Friend WithEvents lbl_6 As Label
    Friend WithEvents Tbl_Lay_7 As TableLayoutPanel
    Friend WithEvents lbl_7 As Label
    Friend WithEvents Tbl_Lay_12 As TableLayoutPanel
    Friend WithEvents lbl_12 As Label
    Friend WithEvents Tbl_Lay_13 As TableLayoutPanel
    Friend WithEvents lbl_13 As Label
    Friend WithEvents Tbl_Lay_14 As TableLayoutPanel
    Friend WithEvents lbl_14 As Label
    Friend WithEvents Tbl_Lay_19 As TableLayoutPanel
    Friend WithEvents lbl_19 As Label
    Friend WithEvents Tbl_Lay_20 As TableLayoutPanel
    Friend WithEvents lbl_20 As Label
    Friend WithEvents Tbl_Lay_21 As TableLayoutPanel
    Friend WithEvents lbl_21 As Label
    Friend WithEvents Tbl_Lay_22 As TableLayoutPanel
    Friend WithEvents lbl_22 As Label
    Friend WithEvents Tbl_Lay_23 As TableLayoutPanel
    Friend WithEvents lbl_23 As Label
    Friend WithEvents Tbl_Lay_24 As TableLayoutPanel
    Friend WithEvents lbl_24 As Label
    Friend WithEvents Tbl_Lay_25 As TableLayoutPanel
    Friend WithEvents lbl_25 As Label
    Friend WithEvents Tbl_Lay_26 As TableLayoutPanel
    Friend WithEvents lbl_26 As Label
    Friend WithEvents Tbl_Lay_27 As TableLayoutPanel
    Friend WithEvents lbl_27 As Label
    Friend WithEvents Tbl_Lay_28 As TableLayoutPanel
    Friend WithEvents lbl_28 As Label
    Friend WithEvents Tbl_Lay_29 As TableLayoutPanel
    Friend WithEvents lbl_29 As Label
    Friend WithEvents lbl_31 As Label
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel8 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel9 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel10 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel11 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel15 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel16 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel17 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel18 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel12 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel13 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel14 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel19 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel20 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel21 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel22 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel23 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel24 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel25 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel26 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel27 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel28 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel29 As FlowLayoutPanel
    Friend WithEvents Tbl_Lay_31 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel35 As FlowLayoutPanel
    Friend WithEvents Panel42 As Panel
    Friend WithEvents Tbl_Lay_42 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel42 As FlowLayoutPanel
    Friend WithEvents lbl_42 As Label
    Friend WithEvents Panel41 As Panel
    Friend WithEvents Tbl_Lay_41 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel41 As FlowLayoutPanel
    Friend WithEvents lbl_41 As Label
    Friend WithEvents Panel40 As Panel
    Friend WithEvents Tbl_Lay_40 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel40 As FlowLayoutPanel
    Friend WithEvents lbl_40 As Label
    Friend WithEvents Panel39 As Panel
    Friend WithEvents Tbl_Lay_39 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel39 As FlowLayoutPanel
    Friend WithEvents lbl_39 As Label
    Friend WithEvents Panel38 As Panel
    Friend WithEvents Tbl_Lay_38 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel38 As FlowLayoutPanel
    Friend WithEvents lbl_38 As Label
    Friend WithEvents Panel37 As Panel
    Friend WithEvents Tbl_Lay_37 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel37 As FlowLayoutPanel
    Friend WithEvents lbl_37 As Label
    Friend WithEvents Panel36 As Panel
    Friend WithEvents Tbl_Lay_36 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel36 As FlowLayoutPanel
    Friend WithEvents lbl_36 As Label
    Friend WithEvents Panel35 As Panel
    Friend WithEvents Tbl_Lay_35 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel34 As FlowLayoutPanel
    Friend WithEvents lbl_35 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Tbl_Lay_1 As TableLayoutPanel
    Friend WithEvents lbl_1 As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents lbl_15 As Label
    Friend WithEvents Tbl_Lay_30 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel30 As FlowLayoutPanel
    Friend WithEvents lbl_30 As Label
    Friend WithEvents Panel32 As Panel
    Friend WithEvents Tbl_Lay_32 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel31 As FlowLayoutPanel
    Friend WithEvents lbl_32 As Label
    Friend WithEvents Panel33 As Panel
    Friend WithEvents Tbl_Lay_33 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel32 As FlowLayoutPanel
    Friend WithEvents lbl_33 As Label
    Friend WithEvents Panel34 As Panel
    Friend WithEvents Tbl_Lay_34 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel33 As FlowLayoutPanel
    Friend WithEvents lbl_34 As Label
End Class
