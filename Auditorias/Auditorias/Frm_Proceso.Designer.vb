﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Proceso
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Proceso))
        Me.txt_auditor = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_auditados = New System.Windows.Forms.TextBox()
        Me.txt_revisa = New System.Windows.Forms.TextBox()
        Me.txt_elabora = New System.Windows.Forms.TextBox()
        Me.dt_fecha = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_guardar = New System.Windows.Forms.Button()
        Me.btn_generar = New System.Windows.Forms.Button()
        Me.txt_3er_turno = New System.Windows.Forms.MaskedTextBox()
        Me.txt_1er_turno = New System.Windows.Forms.MaskedTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_4_ms = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_plan_control = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_ing_estructura = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_ing_dibujo = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_modelo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_nombre_parte = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_numero_parte = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_auditor
        '
        Me.txt_auditor.Location = New System.Drawing.Point(161, 98)
        Me.txt_auditor.Name = "txt_auditor"
        Me.txt_auditor.Size = New System.Drawing.Size(230, 20)
        Me.txt_auditor.TabIndex = 33
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(474, 137)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(122, 16)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "1er. Truno Horario"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(95, 99)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 16)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Auditor:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(474, 188)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(122, 16)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "3er. Truno Horario"
        '
        'txt_auditados
        '
        Me.txt_auditados.Location = New System.Drawing.Point(176, 136)
        Me.txt_auditados.Multiline = True
        Me.txt_auditados.Name = "txt_auditados"
        Me.txt_auditados.Size = New System.Drawing.Size(283, 100)
        Me.txt_auditados.TabIndex = 27
        '
        'txt_revisa
        '
        Me.txt_revisa.Location = New System.Drawing.Point(477, 57)
        Me.txt_revisa.Name = "txt_revisa"
        Me.txt_revisa.Size = New System.Drawing.Size(230, 20)
        Me.txt_revisa.TabIndex = 26
        '
        'txt_elabora
        '
        Me.txt_elabora.Location = New System.Drawing.Point(161, 57)
        Me.txt_elabora.Name = "txt_elabora"
        Me.txt_elabora.Size = New System.Drawing.Size(230, 20)
        Me.txt_elabora.TabIndex = 25
        '
        'dt_fecha
        '
        Me.dt_fecha.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dt_fecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt_fecha.Location = New System.Drawing.Point(605, 93)
        Me.dt_fecha.Name = "dt_fecha"
        Me.dt_fecha.Size = New System.Drawing.Size(113, 22)
        Me.dt_fecha.TabIndex = 24
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(474, 99)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 16)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Fecha:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(95, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 16)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Auditados:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(408, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 16)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Revisa:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(95, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 16)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Elabora:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(283, 22)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Formulario Auditoria Proceso"
        '
        'btn_guardar
        '
        Me.btn_guardar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btn_guardar.Image = Global.Auditorias.My.Resources.Resources.save
        Me.btn_guardar.Location = New System.Drawing.Point(705, 397)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(155, 60)
        Me.btn_guardar.TabIndex = 37
        Me.btn_guardar.Text = "Guardar Documento"
        Me.btn_guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_guardar.UseVisualStyleBackColor = True
        Me.btn_guardar.Visible = False
        '
        'btn_generar
        '
        Me.btn_generar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btn_generar.Image = Global.Auditorias.My.Resources.Resources.audit__2_
        Me.btn_generar.Location = New System.Drawing.Point(12, 397)
        Me.btn_generar.Name = "btn_generar"
        Me.btn_generar.Size = New System.Drawing.Size(155, 60)
        Me.btn_generar.TabIndex = 36
        Me.btn_generar.Text = "Generar Documento"
        Me.btn_generar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_generar.UseVisualStyleBackColor = True
        '
        'txt_3er_turno
        '
        Me.txt_3er_turno.Location = New System.Drawing.Point(605, 187)
        Me.txt_3er_turno.Mask = "00:00 A 00:00"
        Me.txt_3er_turno.Name = "txt_3er_turno"
        Me.txt_3er_turno.Size = New System.Drawing.Size(113, 20)
        Me.txt_3er_turno.TabIndex = 38
        Me.txt_3er_turno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_1er_turno
        '
        Me.txt_1er_turno.Location = New System.Drawing.Point(605, 136)
        Me.txt_1er_turno.Mask = "00:00 A 00:00"
        Me.txt_1er_turno.Name = "txt_1er_turno"
        Me.txt_1er_turno.Size = New System.Drawing.Size(113, 20)
        Me.txt_1er_turno.TabIndex = 39
        Me.txt_1er_turno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_4_ms)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txt_plan_control)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txt_ing_estructura)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txt_ing_dibujo)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txt_modelo)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txt_nombre_parte)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txt_numero_parte)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(33, 242)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(802, 142)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Generales"
        '
        'txt_4_ms
        '
        Me.txt_4_ms.Location = New System.Drawing.Point(552, 75)
        Me.txt_4_ms.Name = "txt_4_ms"
        Me.txt_4_ms.Size = New System.Drawing.Size(205, 22)
        Me.txt_4_ms.TabIndex = 39
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label15.Location = New System.Drawing.Point(351, 80)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(187, 16)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "Ultimo Cambio de 4'M o Ing."
        '
        'txt_plan_control
        '
        Me.txt_plan_control.Location = New System.Drawing.Point(552, 47)
        Me.txt_plan_control.Name = "txt_plan_control"
        Me.txt_plan_control.Size = New System.Drawing.Size(205, 22)
        Me.txt_plan_control.TabIndex = 37
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label14.Location = New System.Drawing.Point(351, 52)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(195, 16)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "Nivel de Ing. Plan de Control:"
        '
        'txt_ing_estructura
        '
        Me.txt_ing_estructura.Location = New System.Drawing.Point(552, 19)
        Me.txt_ing_estructura.Name = "txt_ing_estructura"
        Me.txt_ing_estructura.Size = New System.Drawing.Size(205, 22)
        Me.txt_ing_estructura.TabIndex = 35
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label13.Location = New System.Drawing.Point(351, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(159, 16)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Nivel de Ing. Estructura:"
        '
        'txt_ing_dibujo
        '
        Me.txt_ing_dibujo.Location = New System.Drawing.Point(151, 103)
        Me.txt_ing_dibujo.Name = "txt_ing_dibujo"
        Me.txt_ing_dibujo.Size = New System.Drawing.Size(185, 22)
        Me.txt_ing_dibujo.TabIndex = 33
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(12, 106)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(137, 16)
        Me.Label12.TabIndex = 32
        Me.Label12.Text = "Nivel de Ing. Dibujo:"
        '
        'txt_modelo
        '
        Me.txt_modelo.Location = New System.Drawing.Point(151, 75)
        Me.txt_modelo.Name = "txt_modelo"
        Me.txt_modelo.Size = New System.Drawing.Size(185, 22)
        Me.txt_modelo.TabIndex = 31
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label11.Location = New System.Drawing.Point(12, 78)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 16)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Modelo:"
        '
        'txt_nombre_parte
        '
        Me.txt_nombre_parte.Location = New System.Drawing.Point(151, 47)
        Me.txt_nombre_parte.Name = "txt_nombre_parte"
        Me.txt_nombre_parte.Size = New System.Drawing.Size(185, 22)
        Me.txt_nombre_parte.TabIndex = 29
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(12, 50)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 16)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "Nombre de Parte:"
        '
        'txt_numero_parte
        '
        Me.txt_numero_parte.Location = New System.Drawing.Point(151, 19)
        Me.txt_numero_parte.Name = "txt_numero_parte"
        Me.txt_numero_parte.Size = New System.Drawing.Size(185, 22)
        Me.txt_numero_parte.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(12, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 16)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Numero de Parte:"
        '
        'Frm_Proceso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 469)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txt_1er_turno)
        Me.Controls.Add(Me.txt_3er_turno)
        Me.Controls.Add(Me.btn_guardar)
        Me.Controls.Add(Me.btn_generar)
        Me.Controls.Add(Me.txt_auditor)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_auditados)
        Me.Controls.Add(Me.txt_revisa)
        Me.Controls.Add(Me.txt_elabora)
        Me.Controls.Add(Me.dt_fecha)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Frm_Proceso"
        Me.Text = "Formulario Proceso"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_auditor As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_auditados As TextBox
    Friend WithEvents txt_revisa As TextBox
    Friend WithEvents txt_elabora As TextBox
    Friend WithEvents dt_fecha As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_guardar As Button
    Friend WithEvents btn_generar As Button
    Friend WithEvents txt_3er_turno As MaskedTextBox
    Friend WithEvents txt_1er_turno As MaskedTextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txt_4_ms As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txt_plan_control As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txt_ing_estructura As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txt_ing_dibujo As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txt_modelo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_nombre_parte As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txt_numero_parte As TextBox
    Friend WithEvents Label9 As Label
End Class
