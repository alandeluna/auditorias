﻿Public Class ProgramaAuditoria
    Dim listAudt As List(Of String)
    Dim listAviso As List(Of String)
    Dim dia As String
    Dim mes As String

    Sub New(ByVal dia As String, ByVal mes As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.dia = dia
        Me.mes = mes

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub ProgramaAuditoria_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim dt As New DataTable
        Dim cc As New Clss_ConexionSql
        DateTimePicker1.CustomFormat = "dd/MM/yyyy hh:mm:ss"
        DateTimePicker1.Format = DateTimePickerFormat.Custom

        Me.DateTimePicker1.Text = dia & "/" & mes & "/" & Date.Now.Year
        dt = cc.selectTable("select * from tbTipoAudit").Tables(0)

        For i As Integer = 0 To dt.Rows.Count - 1
            dtgAud.Rows.Add(dt.Rows(i).Item(0), dt.Rows(i).Item(1), False)
        Next

        dt = cc.selectTable("SELECT id_centro_costos, ltrim(rtrim(str_departamento)) str_departamento FROM [NominaDB].[dbo].[RH_departamentos] where Activo = 1").Tables(0)

        With cmbDep
            .DataSource = dt
            .DisplayMember = "str_departamento"
            .ValueMember = "id_centro_costos"
        End With

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim op As New ListaContactos
        op.ShowDialog()

        listAudt = op.list

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim op As New ListaContactos
        op.ShowDialog()

        listAviso = op.list
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim AUDITORES As String = ""
        Dim CONTACTOS As String = ""
        Dim AUDITORIAS As String = ""
        Dim fecha As String = ""
        'MsgBox(Me.DateTimePicker1.Text)
        'fecha = Me.DateTimePicker1.Text.Split("/").GetValue(2) & If(Me.DateTimePicker1.Text.Split("/").GetValue(1).ToString.Length = 1, "0" & Me.DateTimePicker1.Text.Split("/").GetValue(1), Me.DateTimePicker1.Text.Split("/").GetValue(1)) & Me.DateTimePicker1.Text.Split("/").GetValue(0)
        fecha = Me.DateTimePicker1.Text.Substring(6, 4) & Me.DateTimePicker1.Text.Substring(3, 2) & Me.DateTimePicker1.Text.Substring(0, 2) & " " & Me.DateTimePicker1.Text.Substring(11)
        If listAudt.Count <= 0 Then MsgBox("Agrege los Correos de los auditores asignados") : Exit Sub
        If listAviso.Count <= 0 Then MsgBox("Agrege los Correos de los usuarios responsables de la auditoria para el departamento seleccionado") : Exit Sub
        If cmbDep.SelectedIndex <= 0 Then MsgBox("Seleccione un departamento") : Exit Sub

        Dim co As Integer = 0

        For Each row As DataGridViewRow In dtgAud.Rows
            If row.Cells(2).Value = True Then
                AUDITORIAS = AUDITORIAS & row.Cells(1).Value & ";"
                co = co + 1
            End If
        Next

        If co = 0 Then MsgBox("Marque por lo menos 1 tipo de auditoria para aplicar") : Exit Sub

        For I As Integer = 0 To listAudt.Count - 1
            AUDITORES = AUDITORES & listAudt(I) & ";"
        Next

        For I As Integer = 0 To listAviso.Count - 1
            CONTACTOS = CONTACTOS & listAviso(I) & ";"
        Next

        MsgBox("CONTACTOS" & CONTACTOS)
        MsgBox("AUDITORES" & AUDITORES)
        MsgBox("AUDITORIAS" & AUDITORIAS)

        Dim cc As New Clss_ConexionSql
        cc.insert("INSERT INTO [AUDI_PROG] VALUES ('" & txtDes.Text & "','" & fecha & "', '" & AUDITORES & "', '" & CONTACTOS & "', '" & AUDITORIAS & "', '" & cmbDep.SelectedValue & "')")

        MsgBox("Guardado Correctamente")
    End Sub
End Class