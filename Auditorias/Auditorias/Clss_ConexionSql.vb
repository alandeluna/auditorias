﻿Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class Clss_ConexionSql
    Private server As String
    Private database As String
    Private user As String
    Private password As String
    Private strConexion As String
    Private conexion As SqlConnection
    Private dbcomand As SqlCommand
    Private adaptador As SqlDataAdapter
    Sub New()
        Me.server = "SERVER05"
        Me.database = "CONTRO_AUDIT"
        Me.user = "sa"
        Me.password = "adminsa"
        strConexion = "Server=" & server & ";Database=" & database & ";user id=" & user & ";password=" & password & ";Trusted_Connection=False;"
        Me.conexion = New SqlClient.SqlConnection()
        Me.conexion.ConnectionString = strConexion


    End Sub
    Sub New(ByRef srv As String, ByRef db As String, ByRef us As String, ByRef pass As String)
        Me.server = srv
        Me.database = db
        Me.user = us
        Me.password = pass
        strConexion = "Server=" & srv & ";Database=" & db & ";user id=" & us & ";password=" & pass & ";Trusted_Connection=False;"
        conexion = New SqlClient.SqlConnection()
        conexion.ConnectionString = strConexion
    End Sub
    Public Function selectTable(ByRef query As String) As DataSet
        Try
            dbcomand = New SqlCommand(query, conexion)
            dbcomand.CommandTimeout = 0
            'dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.CommandType = CommandType.Text
            If conexion.State = ConnectionState.Closed Then
                conexion.Open()
            End If
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet

            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function
    Public Function selectEnlazado(ByRef query As String)
        Try
            Dim ds As New DataSet
            Dim commandBuilder As SqlCommandBuilder
            dbcomand = New SqlCommand(query, conexion)
            dbcomand.CommandTimeout = 0
            'dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.CommandType = CommandType.Text
            conexion.Open()
            adaptador = New SqlDataAdapter(dbcomand)
            commandBuilder = New SqlCommandBuilder(adaptador)
            adaptador.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function
    Public Function insert(ByRef query As String)
        Try
            dbcomand = New SqlCommand(query, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.Text
            conexion.Open()
            dbcomand.ExecuteNonQuery()
            conexion.Close()
            Return True
        Catch ex As Exception
            MsgBox("Falla en la insercion, " & ex.Message, MsgBoxStyle.Critical)
            Return False
        End Try
    End Function
    Public Function delete(ByRef query As String)
        Try
            dbcomand = New SqlCommand(query, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.Text
            conexion.Open()
            dbcomand.ExecuteNonQuery()
            conexion.Close()
            Return True
        Catch ex As Exception
            MsgBox("Falla en eliminar, " & ex.Message)
            Return False
        End Try
    End Function
    Public Function update(ByRef query As String)
        Try
            dbcomand = New SqlCommand(query, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.Text
            conexion.Open()
            dbcomand.ExecuteNonQuery()
            conexion.Close()
            Return True
        Catch ex As Exception
            MsgBox("Falla en actualizar, " & ex.Message)
            Return False
        End Try
    End Function
    ' 1 Parametro
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar As String, ByRef vVar As String)
        Try

            If conexion.State = ConnectionState.Closed Then
                conexion.Open()
            End If
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar, vVar.Replace("-", ""))
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    '2 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String, ByRef nVar2 As String, ByRef vVar2 As Integer)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String, ByRef nVar2 As String, ByRef vVar2 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    '3 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String, ByRef nVar2 As String,
    ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    '4 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String, ByRef nVar2 As String,
    ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String, ByRef nVar4 As String, ByRef vVar4 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message, MsgBoxStyle.Critical)
            Return Nothing
        End Try

    End Function
    ' 5 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5.Replace(",", "."))
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function

    '6 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function

    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function

    ' 8 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String,
        ByRef nVar8 As String, ByRef vVar8 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            dbcomand.Parameters.AddWithValue("@" & nVar8, vVar8)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function


    '9 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String,
        ByRef nVar8 As String, ByRef vVar8 As String, ByRef nVar9 As String, ByRef vVar9 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            dbcomand.Parameters.AddWithValue("@" & nVar8, vVar8)
            dbcomand.Parameters.AddWithValue("@" & nVar9, vVar9)
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function

    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String,
        ByRef nVar8 As String, ByRef vVar8 As String, ByRef nVar9 As String, ByRef vVar9 As String,
        ByRef nVar10 As String, ByRef vVar10 As String, ByRef nVar11 As String, ByRef vVar11 As String,
        ByRef nVar12 As String, ByRef vVar12 As String, ByRef nVar13 As String, ByRef vVar13 As String,
        ByRef nVar14 As String, ByRef vVar14 As String, ByRef nVar15 As String, ByRef vVar15 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            dbcomand.Parameters.AddWithValue("@" & nVar8, vVar8)
            dbcomand.Parameters.AddWithValue("@" & nVar9, vVar9)
            dbcomand.Parameters.AddWithValue("@" & nVar10, vVar10)
            dbcomand.Parameters.AddWithValue("@" & nVar11, vVar11)
            dbcomand.Parameters.AddWithValue("@" & nVar12, vVar12)
            dbcomand.Parameters.AddWithValue("@" & nVar13, vVar13)
            dbcomand.Parameters.AddWithValue("@" & nVar14, vVar14)
            dbcomand.Parameters.AddWithValue("@" & nVar15, vVar15)

            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function

    '18 paramatros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String,
        ByRef nVar8 As String, ByRef vVar8 As String, ByRef nVar9 As String, ByRef vVar9 As String,
        ByRef nVar10 As String, ByRef vVar10 As String, ByRef nVar11 As String, ByRef vVar11 As String,
        ByRef nVar12 As String, ByRef vVar12 As String, ByRef nVar13 As String, ByRef vVar13 As String,
        ByRef nVar14 As String, ByRef vVar14 As String, ByRef nVar15 As String, ByRef vVar15 As String,
        ByRef nVar16 As String, ByRef vVar16 As String, ByRef nVar17 As String, ByRef vVar17 As String,
        ByRef nVar18 As String, ByRef vVar18 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            dbcomand.Parameters.AddWithValue("@" & nVar8, vVar8)
            dbcomand.Parameters.AddWithValue("@" & nVar9, vVar9)
            dbcomand.Parameters.AddWithValue("@" & nVar10, vVar10)
            dbcomand.Parameters.AddWithValue("@" & nVar11, vVar11)
            dbcomand.Parameters.AddWithValue("@" & nVar12, vVar12)
            dbcomand.Parameters.AddWithValue("@" & nVar13, vVar13)
            dbcomand.Parameters.AddWithValue("@" & nVar14, vVar14)
            dbcomand.Parameters.AddWithValue("@" & nVar15, vVar15)
            dbcomand.Parameters.AddWithValue("@" & nVar16, vVar16)
            dbcomand.Parameters.AddWithValue("@" & nVar17, vVar17)
            dbcomand.Parameters.AddWithValue("@" & nVar18, vVar18)

            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    '19 parametros
    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String,
        ByRef nVar8 As String, ByRef vVar8 As String, ByRef nVar9 As String, ByRef vVar9 As String,
        ByRef nVar10 As String, ByRef vVar10 As String, ByRef nVar11 As String, ByRef vVar11 As String,
        ByRef nVar12 As String, ByRef vVar12 As String, ByRef nVar13 As String, ByRef vVar13 As String,
        ByRef nVar14 As String, ByRef vVar14 As String, ByRef nVar15 As String, ByRef vVar15 As String,
        ByRef nVar16 As String, ByRef vVar16 As String, ByRef nVar17 As String, ByRef vVar17 As String,
        ByRef nVar18 As String, ByRef vVar18 As String, ByRef nVar19 As String, ByRef vVar19 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            dbcomand.Parameters.AddWithValue("@" & nVar8, vVar8)
            dbcomand.Parameters.AddWithValue("@" & nVar9, vVar9)
            dbcomand.Parameters.AddWithValue("@" & nVar10, vVar10)
            dbcomand.Parameters.AddWithValue("@" & nVar11, vVar11)
            dbcomand.Parameters.AddWithValue("@" & nVar12, vVar12)
            dbcomand.Parameters.AddWithValue("@" & nVar13, vVar13)
            dbcomand.Parameters.AddWithValue("@" & nVar14, vVar14)
            dbcomand.Parameters.AddWithValue("@" & nVar15, vVar15)
            dbcomand.Parameters.AddWithValue("@" & nVar16, vVar16)
            dbcomand.Parameters.AddWithValue("@" & nVar17, vVar17)
            dbcomand.Parameters.AddWithValue("@" & nVar18, vVar18)
            dbcomand.Parameters.AddWithValue("@" & nVar19, vVar19)

            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function

    Public Function storedProcedure(ByRef nStored As String, ByRef nVar1 As String, ByRef vVar1 As String,
        ByRef nVar2 As String, ByRef vVar2 As String, ByRef nVar3 As String, ByRef vVar3 As String,
        ByRef nVar4 As String, ByRef vVar4 As String, ByRef nVar5 As String, ByRef vVar5 As String,
        ByRef nVar6 As String, ByRef vVar6 As String, ByRef nVar7 As String, ByRef vVar7 As String,
        ByRef nVar8 As String, ByRef vVar8 As String, ByRef nVar9 As String, ByRef vVar9 As String,
        ByRef nVar10 As String, ByRef vVar10 As String, ByRef nVar11 As String, ByRef vVar11 As String,
        ByRef nVar12 As String, ByRef vVar12 As String, ByRef nVar13 As String, ByRef vVar13 As String,
        ByRef nVar14 As String, ByRef vVar14 As String, ByRef nVar15 As String, ByRef vVar15 As String,
        ByRef nVar16 As String, ByRef vVar16 As String, ByRef nVar17 As String, ByRef vVar17 As String,
        ByRef nVar18 As String, ByRef vVar18 As String, ByRef nVar19 As String, ByRef vVar19 As String,
        ByRef nVar20 As String, ByRef vVar20 As String, ByRef nVar21 As String, ByRef vVar21 As String,
        ByRef nVar22 As String, ByRef vVar22 As String, ByRef nVar23 As String, ByRef vVar23 As String,
        ByRef nVar24 As String, ByRef vVar24 As String, ByRef nVar25 As String, ByRef vVar25 As String,
        ByRef nVar26 As String, ByRef vVar26 As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0

            'vVar4 = vVar4.Split("/").GetValue(2).ToString.Substring(0, 4) + "-" + vVar4.Split("/").GetValue(1) + "-" + vVar4.Split("/").GetValue(0) + " 00:00:00"
            dbcomand.CommandType = CommandType.StoredProcedure
            dbcomand.Parameters.AddWithValue("@" & nVar1, vVar1)
            dbcomand.Parameters.AddWithValue("@" & nVar2, vVar2)
            dbcomand.Parameters.AddWithValue("@" & nVar3, vVar3)
            dbcomand.Parameters.AddWithValue("@" & nVar4, vVar4)
            dbcomand.Parameters.AddWithValue("@" & nVar5, vVar5)
            dbcomand.Parameters.AddWithValue("@" & nVar6, vVar6)
            dbcomand.Parameters.AddWithValue("@" & nVar7, vVar7)
            dbcomand.Parameters.AddWithValue("@" & nVar8, vVar8)
            dbcomand.Parameters.AddWithValue("@" & nVar9, vVar9)
            dbcomand.Parameters.AddWithValue("@" & nVar10, vVar10)
            dbcomand.Parameters.AddWithValue("@" & nVar11, vVar11)
            dbcomand.Parameters.AddWithValue("@" & nVar12, vVar12)
            dbcomand.Parameters.AddWithValue("@" & nVar13, vVar13)
            dbcomand.Parameters.AddWithValue("@" & nVar14, vVar14.Replace("(", "").Replace(")", "").Replace("-", ""))
            dbcomand.Parameters.AddWithValue("@" & nVar15, vVar15.Replace("(", "").Replace(")", "").Replace("-", ""))
            dbcomand.Parameters.AddWithValue("@" & nVar16, vVar16)
            dbcomand.Parameters.AddWithValue("@" & nVar17, vVar17)
            dbcomand.Parameters.AddWithValue("@" & nVar18, vVar18)
            dbcomand.Parameters.AddWithValue("@" & nVar19, vVar19)
            dbcomand.Parameters.AddWithValue("@" & nVar20, vVar20)
            dbcomand.Parameters.AddWithValue("@" & nVar21, vVar21)
            dbcomand.Parameters.AddWithValue("@" & nVar22, vVar22)
            dbcomand.Parameters.AddWithValue("@" & nVar23, vVar23)
            dbcomand.Parameters.AddWithValue("@" & nVar24, vVar24)
            dbcomand.Parameters.AddWithValue("@" & nVar25, vVar25)
            dbcomand.Parameters.AddWithValue("@" & nVar26, vVar26)

            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            'conexion.Close()
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    ' sin parametros
    Public Function storedProcedure(ByRef nStored As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand(nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.StoredProcedure
            Dim dat As New SqlDataAdapter(dbcomand)
            Dim ds As New DataSet
            dat.Fill(ds)
            conexion.Close()
            Return ds
        Catch ex As Exception
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    Public Function storedProcedureExec(ByRef nStored As String)
        Try
            conexion.Open()
            dbcomand = New SqlCommand("exec " & nStored, conexion)
            dbcomand.CommandTimeout = 0
            dbcomand.CommandType = CommandType.Text
            dbcomand.ExecuteNonQuery()
            conexion.Close()
            Return True
        Catch ex As Exception
            MsgBox("Falla de stored : " & ex.Message)
            Return Nothing
        End Try

    End Function
    Public Function copyTable(ByRef tabla As DataTable, ByRef nombreTabla As String)
        Me.conexion.Open()
        Using bulkCopy As SqlBulkCopy =
          New SqlBulkCopy(Me.conexion)
            bulkCopy.DestinationTableName = "dbo." & nombreTabla
            Try

                ' Write from the source to the destination.
                bulkCopy.WriteToServer(tabla)
            Catch ex As Exception
                MsgBox("Error Update Table: " & ex.Message)
                Me.conexion.Close()
                Return False
            End Try
            Me.conexion.Close()
        End Using
        Return True
    End Function
    Public Function UpdateTable(ByRef nwDs As DataTable)
        Try
            Me.adaptador.Update(nwDs)
            Return True
        Catch ex As Exception
            ' MsgBox(ex.Message)
            Return False
        End Try
    End Function
End Class

