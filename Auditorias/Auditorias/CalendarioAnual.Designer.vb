﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CalendarioAnual
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_enero = New System.Windows.Forms.Label()
        Me.Flout_Enero = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_febrero = New System.Windows.Forms.Label()
        Me.Flout_Febrero = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_marzo = New System.Windows.Forms.Label()
        Me.Flout_Marzo = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_abril = New System.Windows.Forms.Label()
        Me.Flout_Abril = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_mayo = New System.Windows.Forms.Label()
        Me.Flout_Mayo = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_junio = New System.Windows.Forms.Label()
        Me.Flout_Junio = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_julio = New System.Windows.Forms.Label()
        Me.Flout_Julio = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_agosto = New System.Windows.Forms.Label()
        Me.Flout_Agosto = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_septiembre = New System.Windows.Forms.Label()
        Me.Flout_Septiembre = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel11 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_octubre = New System.Windows.Forms.Label()
        Me.Flout_Octubre = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel12 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_noviembre = New System.Windows.Forms.Label()
        Me.Flout_Noviembre = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel13 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_diciembre = New System.Windows.Forms.Label()
        Me.Flout_Diciembre = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblMar = New System.Windows.Forms.Label()
        Me.TableLayoutPanel14 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblEne = New System.Windows.Forms.Label()
        Me.lblFeb = New System.Windows.Forms.Label()
        Me.lblAbr = New System.Windows.Forms.Label()
        Me.lblMay = New System.Windows.Forms.Label()
        Me.lblJun = New System.Windows.Forms.Label()
        Me.lblJul = New System.Windows.Forms.Label()
        Me.lblAgo = New System.Windows.Forms.Label()
        Me.lblSep = New System.Windows.Forms.Label()
        Me.lblOct = New System.Windows.Forms.Label()
        Me.lblNov = New System.Windows.Forms.Label()
        Me.lblDic = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Flout_Enero.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Flout_Febrero.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Flout_Marzo.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.Flout_Abril.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Flout_Mayo.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.Flout_Junio.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.Flout_Julio.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.Flout_Agosto.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        Me.Flout_Septiembre.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.TableLayoutPanel11.SuspendLayout()
        Me.Flout_Octubre.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.TableLayoutPanel12.SuspendLayout()
        Me.Flout_Noviembre.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.TableLayoutPanel13.SuspendLayout()
        Me.Flout_Diciembre.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.TableLayoutPanel1.ColumnCount = 4
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel5, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel6, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel7, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel8, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel9, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel10, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel11, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel12, 3, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(784, 562)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.TableLayoutPanel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(190, 134)
        Me.Panel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.lbl_enero, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Flout_Enero, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'lbl_enero
        '
        Me.lbl_enero.AutoSize = True
        Me.lbl_enero.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_enero.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_enero.Location = New System.Drawing.Point(3, 0)
        Me.lbl_enero.Name = "lbl_enero"
        Me.lbl_enero.Size = New System.Drawing.Size(180, 65)
        Me.lbl_enero.TabIndex = 0
        Me.lbl_enero.Text = "Enero"
        Me.lbl_enero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Enero
        '
        Me.Flout_Enero.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Enero.Controls.Add(Me.TableLayoutPanel14)
        Me.Flout_Enero.Controls.Add(Me.lblEne)
        Me.Flout_Enero.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Enero.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Enero.Name = "Flout_Enero"
        Me.Flout_Enero.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Enero.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.TableLayoutPanel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(199, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(190, 134)
        Me.Panel2.TabIndex = 1
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lbl_febrero, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Flout_Febrero, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'lbl_febrero
        '
        Me.lbl_febrero.AutoSize = True
        Me.lbl_febrero.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_febrero.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_febrero.Location = New System.Drawing.Point(3, 0)
        Me.lbl_febrero.Name = "lbl_febrero"
        Me.lbl_febrero.Size = New System.Drawing.Size(180, 65)
        Me.lbl_febrero.TabIndex = 0
        Me.lbl_febrero.Text = "Febrero"
        Me.lbl_febrero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Febrero
        '
        Me.Flout_Febrero.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Febrero.Controls.Add(Me.lblFeb)
        Me.Flout_Febrero.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Febrero.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Febrero.Name = "Flout_Febrero"
        Me.Flout_Febrero.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Febrero.TabIndex = 1
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.TableLayoutPanel4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(395, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(190, 134)
        Me.Panel3.TabIndex = 2
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.lbl_marzo, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Flout_Marzo, 0, 1)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel4.TabIndex = 0
        '
        'lbl_marzo
        '
        Me.lbl_marzo.AutoSize = True
        Me.lbl_marzo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_marzo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_marzo.Location = New System.Drawing.Point(3, 0)
        Me.lbl_marzo.Name = "lbl_marzo"
        Me.lbl_marzo.Size = New System.Drawing.Size(180, 65)
        Me.lbl_marzo.TabIndex = 0
        Me.lbl_marzo.Text = "Marzo"
        Me.lbl_marzo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Marzo
        '
        Me.Flout_Marzo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Marzo.Controls.Add(Me.lblMar)
        Me.Flout_Marzo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Marzo.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Marzo.Name = "Flout_Marzo"
        Me.Flout_Marzo.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Marzo.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel4.Controls.Add(Me.TableLayoutPanel8)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(591, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(190, 134)
        Me.Panel4.TabIndex = 3
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.ColumnCount = 1
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.lbl_abril, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.Flout_Abril, 0, 1)
        Me.TableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 2
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel8.TabIndex = 0
        '
        'lbl_abril
        '
        Me.lbl_abril.AutoSize = True
        Me.lbl_abril.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_abril.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_abril.Location = New System.Drawing.Point(3, 0)
        Me.lbl_abril.Name = "lbl_abril"
        Me.lbl_abril.Size = New System.Drawing.Size(180, 65)
        Me.lbl_abril.TabIndex = 0
        Me.lbl_abril.Text = "Abril"
        Me.lbl_abril.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Abril
        '
        Me.Flout_Abril.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Abril.Controls.Add(Me.lblAbr)
        Me.Flout_Abril.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Abril.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Abril.Name = "Flout_Abril"
        Me.Flout_Abril.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Abril.TabIndex = 1
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel5.Controls.Add(Me.TableLayoutPanel5)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 143)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(190, 134)
        Me.Panel5.TabIndex = 4
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.lbl_mayo, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Flout_Mayo, 0, 1)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 2
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel5.TabIndex = 0
        '
        'lbl_mayo
        '
        Me.lbl_mayo.AutoSize = True
        Me.lbl_mayo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_mayo.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_mayo.Location = New System.Drawing.Point(3, 0)
        Me.lbl_mayo.Name = "lbl_mayo"
        Me.lbl_mayo.Size = New System.Drawing.Size(180, 65)
        Me.lbl_mayo.TabIndex = 0
        Me.lbl_mayo.Text = "Mayo"
        Me.lbl_mayo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Mayo
        '
        Me.Flout_Mayo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Mayo.Controls.Add(Me.lblMay)
        Me.Flout_Mayo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Mayo.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Mayo.Name = "Flout_Mayo"
        Me.Flout_Mayo.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Mayo.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel6.Controls.Add(Me.TableLayoutPanel6)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(199, 143)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(190, 134)
        Me.Panel6.TabIndex = 5
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.lbl_junio, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.Flout_Junio, 0, 1)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 2
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel6.TabIndex = 0
        '
        'lbl_junio
        '
        Me.lbl_junio.AutoSize = True
        Me.lbl_junio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_junio.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_junio.Location = New System.Drawing.Point(3, 0)
        Me.lbl_junio.Name = "lbl_junio"
        Me.lbl_junio.Size = New System.Drawing.Size(180, 65)
        Me.lbl_junio.TabIndex = 0
        Me.lbl_junio.Text = "Junio"
        Me.lbl_junio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Junio
        '
        Me.Flout_Junio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Junio.Controls.Add(Me.lblJun)
        Me.Flout_Junio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Junio.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Junio.Name = "Flout_Junio"
        Me.Flout_Junio.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Junio.TabIndex = 1
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel7.Controls.Add(Me.TableLayoutPanel7)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(395, 143)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(190, 134)
        Me.Panel7.TabIndex = 6
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 1
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.lbl_julio, 0, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.Flout_Julio, 0, 1)
        Me.TableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 2
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel7.TabIndex = 0
        '
        'lbl_julio
        '
        Me.lbl_julio.AutoSize = True
        Me.lbl_julio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_julio.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_julio.Location = New System.Drawing.Point(3, 0)
        Me.lbl_julio.Name = "lbl_julio"
        Me.lbl_julio.Size = New System.Drawing.Size(180, 65)
        Me.lbl_julio.TabIndex = 0
        Me.lbl_julio.Text = "Julio"
        Me.lbl_julio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Julio
        '
        Me.Flout_Julio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Julio.Controls.Add(Me.lblJul)
        Me.Flout_Julio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Julio.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Julio.Name = "Flout_Julio"
        Me.Flout_Julio.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Julio.TabIndex = 1
        '
        'Panel8
        '
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel8.Controls.Add(Me.TableLayoutPanel9)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel8.Location = New System.Drawing.Point(591, 143)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(190, 134)
        Me.Panel8.TabIndex = 7
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 1
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.Controls.Add(Me.lbl_agosto, 0, 0)
        Me.TableLayoutPanel9.Controls.Add(Me.Flout_Agosto, 0, 1)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 2
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel9.TabIndex = 0
        '
        'lbl_agosto
        '
        Me.lbl_agosto.AutoSize = True
        Me.lbl_agosto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_agosto.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_agosto.Location = New System.Drawing.Point(3, 0)
        Me.lbl_agosto.Name = "lbl_agosto"
        Me.lbl_agosto.Size = New System.Drawing.Size(180, 65)
        Me.lbl_agosto.TabIndex = 0
        Me.lbl_agosto.Text = "Agosto"
        Me.lbl_agosto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Agosto
        '
        Me.Flout_Agosto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Agosto.Controls.Add(Me.lblAgo)
        Me.Flout_Agosto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Agosto.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Agosto.Name = "Flout_Agosto"
        Me.Flout_Agosto.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Agosto.TabIndex = 1
        '
        'Panel9
        '
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel9.Controls.Add(Me.TableLayoutPanel10)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(3, 283)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(190, 134)
        Me.Panel9.TabIndex = 8
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.ColumnCount = 1
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.Controls.Add(Me.lbl_septiembre, 0, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.Flout_Septiembre, 0, 1)
        Me.TableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 2
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel10.TabIndex = 0
        '
        'lbl_septiembre
        '
        Me.lbl_septiembre.AutoSize = True
        Me.lbl_septiembre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_septiembre.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_septiembre.Location = New System.Drawing.Point(3, 0)
        Me.lbl_septiembre.Name = "lbl_septiembre"
        Me.lbl_septiembre.Size = New System.Drawing.Size(180, 65)
        Me.lbl_septiembre.TabIndex = 0
        Me.lbl_septiembre.Text = "Semptiembre"
        Me.lbl_septiembre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Septiembre
        '
        Me.Flout_Septiembre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Septiembre.Controls.Add(Me.lblSep)
        Me.Flout_Septiembre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Septiembre.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Septiembre.Name = "Flout_Septiembre"
        Me.Flout_Septiembre.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Septiembre.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel10.Controls.Add(Me.TableLayoutPanel11)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(199, 283)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(190, 134)
        Me.Panel10.TabIndex = 9
        '
        'TableLayoutPanel11
        '
        Me.TableLayoutPanel11.ColumnCount = 1
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel11.Controls.Add(Me.lbl_octubre, 0, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.Flout_Octubre, 0, 1)
        Me.TableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel11.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel11.Name = "TableLayoutPanel11"
        Me.TableLayoutPanel11.RowCount = 2
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel11.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel11.TabIndex = 0
        '
        'lbl_octubre
        '
        Me.lbl_octubre.AutoSize = True
        Me.lbl_octubre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_octubre.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_octubre.Location = New System.Drawing.Point(3, 0)
        Me.lbl_octubre.Name = "lbl_octubre"
        Me.lbl_octubre.Size = New System.Drawing.Size(180, 65)
        Me.lbl_octubre.TabIndex = 0
        Me.lbl_octubre.Text = "Octubre"
        Me.lbl_octubre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Octubre
        '
        Me.Flout_Octubre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Octubre.Controls.Add(Me.lblOct)
        Me.Flout_Octubre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Octubre.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Octubre.Name = "Flout_Octubre"
        Me.Flout_Octubre.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Octubre.TabIndex = 1
        '
        'Panel11
        '
        Me.Panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel11.Controls.Add(Me.TableLayoutPanel12)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel11.Location = New System.Drawing.Point(395, 283)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(190, 134)
        Me.Panel11.TabIndex = 10
        '
        'TableLayoutPanel12
        '
        Me.TableLayoutPanel12.ColumnCount = 1
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.Controls.Add(Me.lbl_noviembre, 0, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Flout_Noviembre, 0, 1)
        Me.TableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel12.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel12.Name = "TableLayoutPanel12"
        Me.TableLayoutPanel12.RowCount = 2
        Me.TableLayoutPanel12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel12.TabIndex = 0
        '
        'lbl_noviembre
        '
        Me.lbl_noviembre.AutoSize = True
        Me.lbl_noviembre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_noviembre.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_noviembre.Location = New System.Drawing.Point(3, 0)
        Me.lbl_noviembre.Name = "lbl_noviembre"
        Me.lbl_noviembre.Size = New System.Drawing.Size(180, 65)
        Me.lbl_noviembre.TabIndex = 0
        Me.lbl_noviembre.Text = "Noviembre"
        Me.lbl_noviembre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Noviembre
        '
        Me.Flout_Noviembre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Noviembre.Controls.Add(Me.lblNov)
        Me.Flout_Noviembre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Noviembre.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Noviembre.Name = "Flout_Noviembre"
        Me.Flout_Noviembre.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Noviembre.TabIndex = 1
        '
        'Panel12
        '
        Me.Panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel12.Controls.Add(Me.TableLayoutPanel13)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel12.Location = New System.Drawing.Point(591, 283)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(190, 134)
        Me.Panel12.TabIndex = 11
        '
        'TableLayoutPanel13
        '
        Me.TableLayoutPanel13.ColumnCount = 1
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel13.Controls.Add(Me.lbl_diciembre, 0, 0)
        Me.TableLayoutPanel13.Controls.Add(Me.Flout_Diciembre, 0, 1)
        Me.TableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel13.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel13.Name = "TableLayoutPanel13"
        Me.TableLayoutPanel13.RowCount = 2
        Me.TableLayoutPanel13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel13.Size = New System.Drawing.Size(186, 130)
        Me.TableLayoutPanel13.TabIndex = 0
        '
        'lbl_diciembre
        '
        Me.lbl_diciembre.AutoSize = True
        Me.lbl_diciembre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl_diciembre.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.lbl_diciembre.Location = New System.Drawing.Point(3, 0)
        Me.lbl_diciembre.Name = "lbl_diciembre"
        Me.lbl_diciembre.Size = New System.Drawing.Size(180, 65)
        Me.lbl_diciembre.TabIndex = 0
        Me.lbl_diciembre.Text = "Diciembre"
        Me.lbl_diciembre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Flout_Diciembre
        '
        Me.Flout_Diciembre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Flout_Diciembre.Controls.Add(Me.lblDic)
        Me.Flout_Diciembre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Flout_Diciembre.Location = New System.Drawing.Point(3, 68)
        Me.Flout_Diciembre.Name = "Flout_Diciembre"
        Me.Flout_Diciembre.Size = New System.Drawing.Size(180, 59)
        Me.Flout_Diciembre.TabIndex = 1
        '
        'lblMar
        '
        Me.lblMar.AutoSize = True
        Me.lblMar.Location = New System.Drawing.Point(3, 0)
        Me.lblMar.Name = "lblMar"
        Me.lblMar.Size = New System.Drawing.Size(13, 13)
        Me.lblMar.TabIndex = 0
        Me.lblMar.Text = "0"
        Me.lblMar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel14
        '
        Me.TableLayoutPanel14.ColumnCount = 2
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel14.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel14.Name = "TableLayoutPanel14"
        Me.TableLayoutPanel14.RowCount = 2
        Me.TableLayoutPanel14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.Size = New System.Drawing.Size(200, 0)
        Me.TableLayoutPanel14.TabIndex = 0
        '
        'lblEne
        '
        Me.lblEne.AutoSize = True
        Me.lblEne.Location = New System.Drawing.Point(3, 6)
        Me.lblEne.Name = "lblEne"
        Me.lblEne.Size = New System.Drawing.Size(13, 13)
        Me.lblEne.TabIndex = 1
        Me.lblEne.Text = "0"
        Me.lblEne.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFeb
        '
        Me.lblFeb.AutoSize = True
        Me.lblFeb.Location = New System.Drawing.Point(3, 0)
        Me.lblFeb.Name = "lblFeb"
        Me.lblFeb.Size = New System.Drawing.Size(13, 13)
        Me.lblFeb.TabIndex = 1
        Me.lblFeb.Text = "0"
        Me.lblFeb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAbr
        '
        Me.lblAbr.AutoSize = True
        Me.lblAbr.Location = New System.Drawing.Point(3, 0)
        Me.lblAbr.Name = "lblAbr"
        Me.lblAbr.Size = New System.Drawing.Size(13, 13)
        Me.lblAbr.TabIndex = 1
        Me.lblAbr.Text = "0"
        Me.lblAbr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMay
        '
        Me.lblMay.AutoSize = True
        Me.lblMay.Location = New System.Drawing.Point(3, 0)
        Me.lblMay.Name = "lblMay"
        Me.lblMay.Size = New System.Drawing.Size(13, 13)
        Me.lblMay.TabIndex = 1
        Me.lblMay.Text = "0"
        Me.lblMay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblJun
        '
        Me.lblJun.AutoSize = True
        Me.lblJun.Location = New System.Drawing.Point(3, 0)
        Me.lblJun.Name = "lblJun"
        Me.lblJun.Size = New System.Drawing.Size(13, 13)
        Me.lblJun.TabIndex = 1
        Me.lblJun.Text = "0"
        Me.lblJun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblJul
        '
        Me.lblJul.AutoSize = True
        Me.lblJul.Location = New System.Drawing.Point(3, 0)
        Me.lblJul.Name = "lblJul"
        Me.lblJul.Size = New System.Drawing.Size(13, 13)
        Me.lblJul.TabIndex = 1
        Me.lblJul.Text = "0"
        Me.lblJul.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAgo
        '
        Me.lblAgo.AutoSize = True
        Me.lblAgo.Location = New System.Drawing.Point(3, 0)
        Me.lblAgo.Name = "lblAgo"
        Me.lblAgo.Size = New System.Drawing.Size(13, 13)
        Me.lblAgo.TabIndex = 1
        Me.lblAgo.Text = "0"
        Me.lblAgo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSep
        '
        Me.lblSep.AutoSize = True
        Me.lblSep.Location = New System.Drawing.Point(3, 0)
        Me.lblSep.Name = "lblSep"
        Me.lblSep.Size = New System.Drawing.Size(13, 13)
        Me.lblSep.TabIndex = 1
        Me.lblSep.Text = "0"
        Me.lblSep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblOct
        '
        Me.lblOct.AutoSize = True
        Me.lblOct.Location = New System.Drawing.Point(3, 0)
        Me.lblOct.Name = "lblOct"
        Me.lblOct.Size = New System.Drawing.Size(13, 13)
        Me.lblOct.TabIndex = 1
        Me.lblOct.Text = "0"
        Me.lblOct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNov
        '
        Me.lblNov.AutoSize = True
        Me.lblNov.Location = New System.Drawing.Point(3, 0)
        Me.lblNov.Name = "lblNov"
        Me.lblNov.Size = New System.Drawing.Size(13, 13)
        Me.lblNov.TabIndex = 1
        Me.lblNov.Text = "0"
        Me.lblNov.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDic
        '
        Me.lblDic.AutoSize = True
        Me.lblDic.Location = New System.Drawing.Point(3, 0)
        Me.lblDic.Name = "lblDic"
        Me.lblDic.Size = New System.Drawing.Size(13, 13)
        Me.lblDic.TabIndex = 1
        Me.lblDic.Text = "0"
        Me.lblDic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CalendarioAnual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 562)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "CalendarioAnual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calendario Anual"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.Flout_Enero.ResumeLayout(False)
        Me.Flout_Enero.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.Flout_Febrero.ResumeLayout(False)
        Me.Flout_Febrero.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.Flout_Marzo.ResumeLayout(False)
        Me.Flout_Marzo.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        Me.Flout_Abril.ResumeLayout(False)
        Me.Flout_Abril.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.Flout_Mayo.ResumeLayout(False)
        Me.Flout_Mayo.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.Flout_Junio.ResumeLayout(False)
        Me.Flout_Junio.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel7.PerformLayout()
        Me.Flout_Julio.ResumeLayout(False)
        Me.Flout_Julio.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.TableLayoutPanel9.PerformLayout()
        Me.Flout_Agosto.ResumeLayout(False)
        Me.Flout_Agosto.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.TableLayoutPanel10.ResumeLayout(False)
        Me.TableLayoutPanel10.PerformLayout()
        Me.Flout_Septiembre.ResumeLayout(False)
        Me.Flout_Septiembre.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.TableLayoutPanel11.ResumeLayout(False)
        Me.TableLayoutPanel11.PerformLayout()
        Me.Flout_Octubre.ResumeLayout(False)
        Me.Flout_Octubre.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.TableLayoutPanel12.ResumeLayout(False)
        Me.TableLayoutPanel12.PerformLayout()
        Me.Flout_Noviembre.ResumeLayout(False)
        Me.Flout_Noviembre.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.TableLayoutPanel13.ResumeLayout(False)
        Me.TableLayoutPanel13.PerformLayout()
        Me.Flout_Diciembre.ResumeLayout(False)
        Me.Flout_Diciembre.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents lbl_enero As Label
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lbl_febrero As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents lbl_marzo As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents TableLayoutPanel8 As TableLayoutPanel
    Friend WithEvents lbl_abril As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents TableLayoutPanel5 As TableLayoutPanel
    Friend WithEvents lbl_mayo As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents TableLayoutPanel6 As TableLayoutPanel
    Friend WithEvents lbl_junio As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents TableLayoutPanel7 As TableLayoutPanel
    Friend WithEvents lbl_julio As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents TableLayoutPanel9 As TableLayoutPanel
    Friend WithEvents lbl_agosto As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents TableLayoutPanel10 As TableLayoutPanel
    Friend WithEvents lbl_septiembre As Label
    Friend WithEvents Panel10 As Panel
    Friend WithEvents TableLayoutPanel11 As TableLayoutPanel
    Friend WithEvents lbl_octubre As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents TableLayoutPanel12 As TableLayoutPanel
    Friend WithEvents lbl_noviembre As Label
    Friend WithEvents Panel12 As Panel
    Friend WithEvents TableLayoutPanel13 As TableLayoutPanel
    Friend WithEvents lbl_diciembre As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Flout_Enero As FlowLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Flout_Febrero As FlowLayoutPanel
    Friend WithEvents Flout_Marzo As FlowLayoutPanel
    Friend WithEvents Flout_Abril As FlowLayoutPanel
    Friend WithEvents Flout_Mayo As FlowLayoutPanel
    Friend WithEvents Flout_Junio As FlowLayoutPanel
    Friend WithEvents Flout_Julio As FlowLayoutPanel
    Friend WithEvents Flout_Agosto As FlowLayoutPanel
    Friend WithEvents Flout_Septiembre As FlowLayoutPanel
    Friend WithEvents Flout_Octubre As FlowLayoutPanel
    Friend WithEvents Flout_Noviembre As FlowLayoutPanel
    Friend WithEvents Flout_Diciembre As FlowLayoutPanel
    Friend WithEvents lblMar As Label
    Friend WithEvents TableLayoutPanel14 As TableLayoutPanel
    Friend WithEvents lblEne As Label
    Friend WithEvents lblFeb As Label
    Friend WithEvents lblAbr As Label
    Friend WithEvents lblMay As Label
    Friend WithEvents lblJun As Label
    Friend WithEvents lblJul As Label
    Friend WithEvents lblAgo As Label
    Friend WithEvents lblSep As Label
    Friend WithEvents lblOct As Label
    Friend WithEvents lblNov As Label
    Friend WithEvents lblDic As Label
End Class
