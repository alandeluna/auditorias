﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop.Excel
Public Class Frm_Proceso

    Dim archivo As String

    Sub New(ByVal archivo As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.archivo = archivo
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btn_generar_Click(sender As Object, e As EventArgs) Handles btn_generar.Click
        Dim app As Excel.Application = New Excel.Application
        Dim libro As Excel.Workbook
        Dim hoja As Excel.Worksheet
        libro = app.Workbooks.Open(Me.archivo)
        hoja = app.Worksheets(1)

        hoja.Range("E3").Value = UCase(Me.txt_elabora.Text) & Chr(10) & "AUDITOR"
        hoja.Range("E3").Interior.Color = Color.LightGreen

        hoja.Range("J3").Value = UCase(Me.txt_revisa.Text) & Chr(10) & "SUPERVISOR"
        hoja.Range("J3").Interior.Color = Color.LightGreen

        hoja.Range("C8").Value = UCase(Me.txt_auditor.Text)
        hoja.Range("C8").Interior.Color = Color.LightGreen

        hoja.Range("C9").Value = UCase(Me.txt_auditados.Text)
        hoja.Range("C9").Interior.Color = Color.LightGreen

        hoja.Range("J8").Value = UCase(CDate(Me.dt_fecha.Value).Date)
        hoja.Range("J8").Interior.Color = Color.LightGreen

        hoja.Range("J9").Value = UCase(Me.txt_1er_turno.Text)
        hoja.Range("J9").Interior.Color = Color.LightGreen

        hoja.Range("J11").Value = UCase(Me.txt_3er_turno.Text)
        hoja.Range("J11").Interior.Color = Color.LightGreen

        hoja.Range("C16").Value = UCase(Me.txt_numero_parte.Text)
        hoja.Range("C16").Interior.Color = Color.LightGreen

        hoja.Range("C17").Value = UCase(Me.txt_nombre_parte.Text)
        hoja.Range("C17").Interior.Color = Color.LightGreen

        hoja.Range("C18").Value = UCase(Me.txt_modelo.Text)
        hoja.Range("C18").Interior.Color = Color.LightGreen

        hoja.Range("C19").Value = UCase(Me.txt_ing_dibujo.Text)
        hoja.Range("C19").Interior.Color = Color.LightGreen

        hoja.Range("C20").Value = UCase(Me.txt_ing_estructura.Text)
        hoja.Range("C20").Interior.Color = Color.LightGreen

        hoja.Range("C21").Value = UCase(Me.txt_plan_control.Text)
        hoja.Range("C21").Interior.Color = Color.LightGreen

        hoja.Range("D22").Value = UCase(Me.txt_4_ms.Text)
        hoja.Range("D22").Interior.Color = Color.LightGreen


        app.ActiveWindow.View = XlWindowView.xlPageLayoutView
        libro.Close(SaveChanges:=True)
        app.Quit()

        releaseObject(app)
        releaseObject(libro)
        releaseObject(hoja)

        Me.btn_guardar.Visible = True
        Me.btn_generar.Enabled = False
        Process.Start(Me.archivo)

    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        If IsFileOpen(Me.archivo) = False Then
            MsgBox("Registro Guardado")
        Else
            MsgBox("El Documento Se Encuentra Abierto; LLenalo, Guardalo y Cierralo Para Poder Continuar...!!", MsgBoxStyle.Information)
        End If
    End Sub

    Private Function IsFileOpen(ByVal filePath As String) As Boolean
        Dim rtnvalue As Boolean = False
        Try
            Dim fs As System.IO.FileStream = System.IO.File.OpenWrite(filePath)
            fs.Close()
        Catch ex As System.IO.IOException
            rtnvalue = True
        End Try
        Return rtnvalue
    End Function

End Class