﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop.Excel
Public Class Frm_Sistema
    Dim archivo As String

    Sub New(ByVal archivo As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.archivo = archivo
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_generar.Click

        Dim app As Excel.Application = New Excel.Application
        Dim libro As Excel.Workbook
        Dim hoja As Excel.Worksheet
        libro = app.Workbooks.Open(Me.archivo)
        hoja = app.Worksheets(1)

        hoja.Range("A9").Value = UCase(Me.txt_area_auditada.Text)
        hoja.Range("A9").Interior.Color = Color.LightGreen

        hoja.Range("C9").Value = UCase(Me.txt_responsable_area.Text)
        hoja.Range("C9").Interior.Color = Color.LightGreen

        hoja.Range("F9").Value = UCase(Me.txt_auditados.Text)
        hoja.Range("F9").Interior.Color = Color.LightGreen

        hoja.Range("H6").Value = UCase(CDate(Me.dt_fecha.Value).Date)
        hoja.Range("H6").Interior.Color = Color.LightGreen

        hoja.Range("A64").Value = "AUDITOR: " & UCase(Me.txt_auditor.Text)
        hoja.Range("A64").Interior.Color = Color.LightGreen

        hoja.Range("D64").Value = "RESP. DEL ÁREA: " & UCase(Me.txt_responsable_area_2.Text)
        hoja.Range("D64").Interior.Color = Color.LightGreen

        hoja.Range("G64").Value = "AUDITOR LÍDER: " & UCase(Me.txt_auditor_lider.Text)
        hoja.Range("G64").Interior.Color = Color.LightGreen

        hoja.Range("A11").Value = "PROCESO A AUDITAR: " & UCase(Me.txt_proceso_a_auditar.Text)
        hoja.Range("A11").Interior.Color = Color.LightGreen

        app.ActiveWindow.View = XlWindowView.xlPageLayoutView
        libro.Close(SaveChanges:=True)
        app.Quit()

        releaseObject(app)
        releaseObject(libro)
        releaseObject(hoja)

        Me.btn_guardar.Visible = True
        Me.btn_generar.Enabled = False
        Process.Start(Me.archivo)

    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        If IsFileOpen(Me.archivo) = False Then
            MsgBox("Registro Guardado")
        Else
            MsgBox("El Documento Se Encuentra Abierto; LLenalo, Guardalo y Cierralo Para Poder Continuar...!!", MsgBoxStyle.Information)
        End If
    End Sub

    Private Function IsFileOpen(ByVal filePath As String) As Boolean
        Dim rtnvalue As Boolean = False
        Try
            Dim fs As System.IO.FileStream = System.IO.File.OpenWrite(filePath)
            fs.Close()
        Catch ex As System.IO.IOException
            rtnvalue = True
        End Try
        Return rtnvalue
    End Function
End Class