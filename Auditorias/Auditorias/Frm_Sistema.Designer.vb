﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Sistema
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Sistema))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dt_fecha = New System.Windows.Forms.DateTimePicker()
        Me.txt_area_auditada = New System.Windows.Forms.TextBox()
        Me.txt_responsable_area = New System.Windows.Forms.TextBox()
        Me.txt_auditados = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_proceso_a_auditar = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_auditor_lider = New System.Windows.Forms.TextBox()
        Me.txt_responsable_area_2 = New System.Windows.Forms.TextBox()
        Me.txt_auditor = New System.Windows.Forms.TextBox()
        Me.btn_guardar = New System.Windows.Forms.Button()
        Me.btn_generar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(279, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Formulario Auditoria Sistema"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(62, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Area Auditada"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(275, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(147, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Responsable del Area"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(553, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Auditados"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(489, 29)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Fecha"
        '
        'dt_fecha
        '
        Me.dt_fecha.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dt_fecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt_fecha.Location = New System.Drawing.Point(542, 24)
        Me.dt_fecha.Name = "dt_fecha"
        Me.dt_fecha.Size = New System.Drawing.Size(136, 22)
        Me.dt_fecha.TabIndex = 5
        '
        'txt_area_auditada
        '
        Me.txt_area_auditada.Location = New System.Drawing.Point(16, 92)
        Me.txt_area_auditada.Name = "txt_area_auditada"
        Me.txt_area_auditada.Size = New System.Drawing.Size(205, 20)
        Me.txt_area_auditada.TabIndex = 6
        '
        'txt_responsable_area
        '
        Me.txt_responsable_area.Location = New System.Drawing.Point(247, 92)
        Me.txt_responsable_area.Name = "txt_responsable_area"
        Me.txt_responsable_area.Size = New System.Drawing.Size(212, 20)
        Me.txt_responsable_area.TabIndex = 7
        '
        'txt_auditados
        '
        Me.txt_auditados.Location = New System.Drawing.Point(482, 92)
        Me.txt_auditados.Name = "txt_auditados"
        Me.txt_auditados.Size = New System.Drawing.Size(212, 20)
        Me.txt_auditados.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(13, 130)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Proceso a Auditar"
        '
        'txt_proceso_a_auditar
        '
        Me.txt_proceso_a_auditar.Location = New System.Drawing.Point(140, 129)
        Me.txt_proceso_a_auditar.Name = "txt_proceso_a_auditar"
        Me.txt_proceso_a_auditar.Size = New System.Drawing.Size(554, 20)
        Me.txt_proceso_a_auditar.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(76, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 16)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Auditor:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(275, 178)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(151, 16)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Responsable del Area:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(553, 178)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(91, 16)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Auditor Lider"
        '
        'txt_auditor_lider
        '
        Me.txt_auditor_lider.Location = New System.Drawing.Point(482, 197)
        Me.txt_auditor_lider.Name = "txt_auditor_lider"
        Me.txt_auditor_lider.Size = New System.Drawing.Size(212, 20)
        Me.txt_auditor_lider.TabIndex = 16
        '
        'txt_responsable_area_2
        '
        Me.txt_responsable_area_2.Location = New System.Drawing.Point(247, 197)
        Me.txt_responsable_area_2.Name = "txt_responsable_area_2"
        Me.txt_responsable_area_2.Size = New System.Drawing.Size(212, 20)
        Me.txt_responsable_area_2.TabIndex = 15
        '
        'txt_auditor
        '
        Me.txt_auditor.Location = New System.Drawing.Point(16, 197)
        Me.txt_auditor.Name = "txt_auditor"
        Me.txt_auditor.Size = New System.Drawing.Size(205, 20)
        Me.txt_auditor.TabIndex = 14
        '
        'btn_guardar
        '
        Me.btn_guardar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btn_guardar.Image = Global.Auditorias.My.Resources.Resources.save
        Me.btn_guardar.Location = New System.Drawing.Point(557, 240)
        Me.btn_guardar.Name = "btn_guardar"
        Me.btn_guardar.Size = New System.Drawing.Size(155, 60)
        Me.btn_guardar.TabIndex = 18
        Me.btn_guardar.Text = "Guardar Documento"
        Me.btn_guardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_guardar.UseVisualStyleBackColor = True
        Me.btn_guardar.Visible = False
        '
        'btn_generar
        '
        Me.btn_generar.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btn_generar.Image = Global.Auditorias.My.Resources.Resources.audit__2_
        Me.btn_generar.Location = New System.Drawing.Point(16, 240)
        Me.btn_generar.Name = "btn_generar"
        Me.btn_generar.Size = New System.Drawing.Size(155, 60)
        Me.btn_generar.TabIndex = 17
        Me.btn_generar.Text = "Generar Documento"
        Me.btn_generar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_generar.UseVisualStyleBackColor = True
        '
        'Frm_Sistema
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 312)
        Me.Controls.Add(Me.btn_guardar)
        Me.Controls.Add(Me.btn_generar)
        Me.Controls.Add(Me.txt_auditor_lider)
        Me.Controls.Add(Me.txt_responsable_area_2)
        Me.Controls.Add(Me.txt_auditor)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_proceso_a_auditar)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_auditados)
        Me.Controls.Add(Me.txt_responsable_area)
        Me.Controls.Add(Me.txt_area_auditada)
        Me.Controls.Add(Me.dt_fecha)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Frm_Sistema"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Formulario Sistema"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents dt_fecha As DateTimePicker
    Friend WithEvents txt_area_auditada As TextBox
    Friend WithEvents txt_responsable_area As TextBox
    Friend WithEvents txt_auditados As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_proceso_a_auditar As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_auditor_lider As TextBox
    Friend WithEvents txt_responsable_area_2 As TextBox
    Friend WithEvents txt_auditor As TextBox
    Friend WithEvents btn_generar As Button
    Friend WithEvents btn_guardar As Button
End Class
