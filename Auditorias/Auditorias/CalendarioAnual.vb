﻿Public Class CalendarioAnual
    Dim Frm_Calendario_Mensual As Calendario_Mensual

    Private Sub lbl_enero_Click(sender As Object, e As EventArgs) Handles lbl_enero.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/01/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_febrero_Click(sender As Object, e As EventArgs) Handles lbl_febrero.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/02/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_marzo_Click(sender As Object, e As EventArgs) Handles lbl_marzo.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/03/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_abril_Click(sender As Object, e As EventArgs) Handles lbl_abril.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/04/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_mayo_Click(sender As Object, e As EventArgs) Handles lbl_mayo.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/05/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_junio_Click(sender As Object, e As EventArgs) Handles lbl_junio.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/06/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_julio_Click(sender As Object, e As EventArgs) Handles lbl_julio.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/07/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_agosto_Click(sender As Object, e As EventArgs) Handles lbl_agosto.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/08/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub
    Private Sub lbl_septiembre_Click(sender As Object, e As EventArgs) Handles lbl_septiembre.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/09/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub
    Private Sub lbl_octubre_Click(sender As Object, e As EventArgs) Handles lbl_octubre.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/10/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub
    Private Sub lbl_noviembre_Click(sender As Object, e As EventArgs) Handles lbl_noviembre.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/11/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub lbl_diciembre_Click(sender As Object, e As EventArgs) Handles lbl_diciembre.Click
        Dim fecha As DateTime = Date.Now.Date.ToString("01/12/yyyy")
        Me.Frm_Calendario_Mensual = New Calendario_Mensual(fecha)
        Frm_Calendario_Mensual.Show()
    End Sub

    Private Sub CalendarioAnual_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim cc As New Clss_ConexionSql
        Dim dt As New DataTable

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200101' AND '20200130'").Tables(0)

        lblEne.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200201' AND '20200229'").Tables(0)

        lblFeb.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200301' AND '20200330'").Tables(0)

        lblMar.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200401' AND '20200430'").Tables(0)

        lblAbr.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200501' AND '20200530'").Tables(0)

        lblMay.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200601' AND '20200630'").Tables(0)

        lblJun.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200701' AND '20200730'").Tables(0)

        lblJul.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200801' AND '20200830'").Tables(0)

        lblAgo.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20200901' AND '20200930'").Tables(0)

        lblSep.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20201001' AND '20201030'").Tables(0)

        lblOct.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20201101' AND '20201130'").Tables(0)

        lblNov.Text = dt.Rows(0).Item(0)

        dt = cc.selectTable("SELECT COUNT(1) FROM [CONTRO_AUDIT].[dbo].[AUDI_PROG] WHERE FECHA BETWEEN '20201201' AND '20201230'").Tables(0)

        lblDic.Text = dt.Rows(0).Item(0)

    End Sub
End Class
