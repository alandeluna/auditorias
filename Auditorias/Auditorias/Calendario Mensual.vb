﻿
Public Class Calendario_Mensual

    Dim fecha_inicio As DateTime
    Dim fecha_fin As DateTime
    Dim formLabels() As Label = {}
    Dim formTableLayOut() As TableLayoutPanel = {}
    Dim Frm_Programacion_Auditorias As ProgramaAuditoria

    Sub New(ByRef fecha As DateTime)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.fecha_inicio = fecha.Date.ToString("dd-MM-yyyy")
        Me.fecha_fin = fecha_inicio.Date.AddMonths(1).AddDays(-1)

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub Calendario_Mensual_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        llenar_arrays(UCase(Me.fecha_inicio.Date.ToString("dddd")))
        While True
            llenar_cuadro_mes(Me.fecha_inicio.Day, UCase(Me.fecha_inicio.Date.ToString("dddd")))
            Me.fecha_inicio = Me.fecha_inicio.AddDays(1)
            If fecha_inicio = fecha_fin.AddDays(1) Then
                Exit While
            End If
        End While
    End Sub

    Private Sub llenar_cuadro_mes(ByVal dia As Integer, ByVal nom_dia As String)

        formLabels(dia - 1).Text = dia & vbCrLf & nom_dia
        formLabels(dia - 1).Visible = True
        formTableLayOut(dia - 1).Visible = True

    End Sub

    Private Sub llenar_arrays(ByVal nom_dia As String)
        Select Case nom_dia
            Case "LUNES"
                formLabels = {lbl_1, lbl_2, lbl_3, lbl_4, lbl_5, lbl_6, lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_1, Tbl_Lay_2, Tbl_Lay_3, Tbl_Lay_4, Tbl_Lay_5, Tbl_Lay_6, Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
            Case "MARTES"
                formLabels = {lbl_2, lbl_3, lbl_4, lbl_5, lbl_6, lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_2, Tbl_Lay_3, Tbl_Lay_4, Tbl_Lay_5, Tbl_Lay_6, Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
            Case "MIÉRCOLES"
                formLabels = {lbl_3, lbl_4, lbl_5, lbl_6, lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_3, Tbl_Lay_4, Tbl_Lay_5, Tbl_Lay_6, Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
            Case "JUEVES"
                formLabels = {lbl_4, lbl_5, lbl_6, lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_4, Tbl_Lay_5, Tbl_Lay_6, Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
            Case "VIERNES"
                formLabels = {lbl_5, lbl_6, lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_5, Tbl_Lay_6, Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
            Case "SÁBADO"
                formLabels = {lbl_6, lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_6, Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
            Case "DOMINGO"
                formLabels = {lbl_7, lbl_8, lbl_9, lbl_10, lbl_11, lbl_12, lbl_13, lbl_14, lbl_15,
                    lbl_16, lbl_17, lbl_18, lbl_19, lbl_20, lbl_21, lbl_22, lbl_23, lbl_24, lbl_25, lbl_26, lbl_27, lbl_28, lbl_29, lbl_30,
                    lbl_31, lbl_32, lbl_33, lbl_34, lbl_35, lbl_36, lbl_37, lbl_38, lbl_39, lbl_40, lbl_41, lbl_42}

                formTableLayOut = {Tbl_Lay_7, Tbl_Lay_8, Tbl_Lay_9, Tbl_Lay_10, Tbl_Lay_11, Tbl_Lay_12, Tbl_Lay_13, Tbl_Lay_14, Tbl_Lay_15,
                    Tbl_Lay_16, Tbl_Lay_17, Tbl_Lay_18, Tbl_Lay_19, Tbl_Lay_20, Tbl_Lay_21, Tbl_Lay_22, Tbl_Lay_23, Tbl_Lay_24, Tbl_Lay_25, Tbl_Lay_26, Tbl_Lay_27, Tbl_Lay_28, Tbl_Lay_29, Tbl_Lay_30,
                    Tbl_Lay_31, Tbl_Lay_32, Tbl_Lay_33, Tbl_Lay_34, Tbl_Lay_35, Tbl_Lay_36, Tbl_Lay_37, Tbl_Lay_38, Tbl_Lay_39, Tbl_Lay_40, Tbl_Lay_41, Tbl_Lay_42}
        End Select
    End Sub

    Private Sub lbl_1_Click(sender As Object, e As EventArgs) Handles lbl_1.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_1.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_2_Click(sender As Object, e As EventArgs) Handles lbl_2.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_2.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_3_Click(sender As Object, e As EventArgs) Handles lbl_3.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_3.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_4_Click(sender As Object, e As EventArgs) Handles lbl_4.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_4.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_5_Click(sender As Object, e As EventArgs) Handles lbl_5.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_5.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_6_Click(sender As Object, e As EventArgs) Handles lbl_6.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_6.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_7_Click(sender As Object, e As EventArgs) Handles lbl_7.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_7.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_8_Click(sender As Object, e As EventArgs) Handles lbl_8.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_8.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_9_Click(sender As Object, e As EventArgs) Handles lbl_9.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_9.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_10_Click(sender As Object, e As EventArgs) Handles lbl_10.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_10.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_11_Click(sender As Object, e As EventArgs) Handles lbl_11.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_11.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_12_Click(sender As Object, e As EventArgs) Handles lbl_12.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_12.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_13_Click(sender As Object, e As EventArgs) Handles lbl_13.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_13.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_14_Click(sender As Object, e As EventArgs) Handles lbl_14.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_14.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_15_Click(sender As Object, e As EventArgs) Handles lbl_15.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_15.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_16_Click(sender As Object, e As EventArgs) Handles lbl_16.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_16.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_17_Click(sender As Object, e As EventArgs) Handles lbl_17.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_17.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_18_Click(sender As Object, e As EventArgs) Handles lbl_18.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_18.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_19_Click(sender As Object, e As EventArgs) Handles lbl_19.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_19.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_20_Click(sender As Object, e As EventArgs) Handles lbl_20.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_20.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_21_Click(sender As Object, e As EventArgs) Handles lbl_21.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_21.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_22_Click(sender As Object, e As EventArgs) Handles lbl_22.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_22.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_23_Click(sender As Object, e As EventArgs) Handles lbl_23.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_23.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_24_Click(sender As Object, e As EventArgs) Handles lbl_24.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_24.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_25_Click(sender As Object, e As EventArgs) Handles lbl_25.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_25.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_26_Click(sender As Object, e As EventArgs) Handles lbl_26.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_26.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_27_Click(sender As Object, e As EventArgs) Handles lbl_27.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_27.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_28_Click(sender As Object, e As EventArgs) Handles lbl_28.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_28.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_29_Click(sender As Object, e As EventArgs) Handles lbl_29.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_29.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_30_Click(sender As Object, e As EventArgs) Handles lbl_30.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_30.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub


    Private Sub lbl_31_Click(sender As Object, e As EventArgs) Handles lbl_31.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_31.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub


    Private Sub lbl_32_Click(sender As Object, e As EventArgs) Handles lbl_32.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_32.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_33_Click(sender As Object, e As EventArgs) Handles lbl_33.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_33.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_34_Click(sender As Object, e As EventArgs) Handles lbl_34.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_34.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_35_Click(sender As Object, e As EventArgs) Handles lbl_35.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_35.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_36_Click(sender As Object, e As EventArgs) Handles lbl_36.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_36.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_37_Click(sender As Object, e As EventArgs) Handles lbl_37.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_37.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_38_Click(sender As Object, e As EventArgs) Handles lbl_38.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_38.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_39_Click(sender As Object, e As EventArgs) Handles lbl_39.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_39.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_40_Click(sender As Object, e As EventArgs) Handles lbl_40.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_40.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_41_Click(sender As Object, e As EventArgs) Handles lbl_41.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_41.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub

    Private Sub lbl_42_Click(sender As Object, e As EventArgs) Handles lbl_42.Click
        Frm_Programacion_Auditorias = New ProgramaAuditoria(Replace(Me.lbl_42.Text.Substring(0, 2), Chr(13), ""), fecha_inicio.AddMonths(-1).Month)
        Frm_Programacion_Auditorias.ShowDialog()
    End Sub
End Class