﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Auditorias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cmbDep = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dt_del = New System.Windows.Forms.DateTimePicker()
        Me.dt_al = New System.Windows.Forms.DateTimePicker()
        Me.dtgAud = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.dtgAud, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbDep
        '
        Me.cmbDep.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbDep.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cmbDep.FormattingEnabled = True
        Me.cmbDep.Items.AddRange(New Object() {"611 - Ingenieria", "211 - Sistemas", "001 - Todos"})
        Me.cmbDep.Location = New System.Drawing.Point(208, 46)
        Me.cmbDep.Name = "cmbDep"
        Me.cmbDep.Size = New System.Drawing.Size(347, 24)
        Me.cmbDep.TabIndex = 5
        Me.cmbDep.Text = "001 - Todos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(100, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Departamento:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(100, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Del:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(301, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Al:"
        '
        'dt_del
        '
        Me.dt_del.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dt_del.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt_del.Location = New System.Drawing.Point(139, 12)
        Me.dt_del.Name = "dt_del"
        Me.dt_del.Size = New System.Drawing.Size(156, 22)
        Me.dt_del.TabIndex = 8
        '
        'dt_al
        '
        Me.dt_al.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.dt_al.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt_al.Location = New System.Drawing.Point(332, 12)
        Me.dt_al.Name = "dt_al"
        Me.dt_al.Size = New System.Drawing.Size(156, 22)
        Me.dt_al.TabIndex = 9
        '
        'dtgAud
        '
        Me.dtgAud.AllowUserToAddRows = False
        Me.dtgAud.AllowUserToDeleteRows = False
        Me.dtgAud.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dtgAud.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgAud.Location = New System.Drawing.Point(12, 79)
        Me.dtgAud.Name = "dtgAud"
        Me.dtgAud.ReadOnly = True
        Me.dtgAud.RowHeadersVisible = False
        Me.dtgAud.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgAud.Size = New System.Drawing.Size(627, 212)
        Me.dtgAud.TabIndex = 10
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Button1.Image = Global.Auditorias.My.Resources.Resources.audit__2_
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(256, 297)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(126, 43)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Llenar Formulario"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Auditorias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 352)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dtgAud)
        Me.Controls.Add(Me.dt_al)
        Me.Controls.Add(Me.dt_del)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbDep)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "Auditorias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auditorias"
        CType(Me.dtgAud, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmbDep As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents dt_del As DateTimePicker
    Friend WithEvents dt_al As DateTimePicker
    Friend WithEvents dtgAud As DataGridView
    Friend WithEvents Button1 As Button
End Class
