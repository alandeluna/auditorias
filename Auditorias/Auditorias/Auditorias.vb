﻿Imports System.IO
Imports Excel = Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop.Excel
Public Class Auditorias
    Dim conexion As Clss_ConexionSql
    Dim id As Integer
    Dim tipo_auditoria As String
    Dim file As String
    Dim Frm_sistemas As Frm_Sistema
    Dim Frm_procesos As Frm_Proceso

    Private Sub Auditorias_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        Dim fechainicio As New Date
        fechainicio = Date.Now.Date
        If fechainicio.DayOfWeek = 7 Then
            fechainicio.AddDays(1)
            Me.dt_del.Value = fechainicio.ToString("dd") & Date.Now.Date.ToString("/MM/yyyy")
            Me.dt_al.Value = fechainicio.AddDays(6).ToString("dd") & Date.Now.Date.ToString("/MM/yyyy")
        Else
            fechainicio = fechainicio.AddDays(-(fechainicio.DayOfWeek - 1))
            Me.dt_del.Value = fechainicio.ToString("dd") & Date.Now.Date.ToString("/MM/yyyy")
            Me.dt_al.Value = fechainicio.AddDays(6).ToString("dd") & Date.Now.Date.ToString("/MM/yyyy")
        End If

        conexion = New Clss_ConexionSql
        Dim ds As DataSet
        ds = conexion.selectTable("select '001' id_centro_costos, '001 - TODOS' str_departamento
                                    UNION
                                    SELECT  id_centro_costos, id_centro_costos + ' - ' + ltrim(rtrim(str_departamento)) str_departamento FROM [NominaDB].[dbo].[RH_departamentos] where Activo = 1")

        With cmbDep
            .DataSource = ds.Tables(0)
            .DisplayMember = "str_departamento"
            .ValueMember = "id_centro_costos"
        End With


        ds = conexion.selectTable("select ID, TIPO_AUDIT as Auditoria, ID_DEPART as Departamento, FECHA as Fecha, DESCR as Descripcion from [AUDI_PROG] where fecha between '" & Me.dt_del.Value.Date.Year & fecha(Me.dt_del.Value.Date.Month) & fecha(Me.dt_del.Value.Date.Day) & "' and '" & Me.dt_al.Value.Date.Year & fecha(Me.dt_al.Value.Date.Month) & fecha(Me.dt_al.Value.Date.Day) & "'")
        If ds IsNot Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("Auditoria") = "" Then

                End If
                dtgAud.DataSource = ds.Tables(0)
                End If
            Else
            conexion = New Clss_ConexionSql
        End If
    End Sub

    Private Function fecha(ByVal a As String)
        Dim b As String
        Select Case a
            Case "1"
                b = "01"
            Case "2"
                b = "02"
            Case "3"
                b = "03"
            Case "4"
                b = "04"
            Case "5"
                b = "05"
            Case "6"
                b = "06"
            Case "7"
                b = "07"
            Case "8"
                b = "08"
            Case "9"
                b = "09"
            Case Else
                b = a
        End Select
        Return b
    End Function
    Private Sub dtgAud_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgAud.CellContentClick
        Dim row As DataGridViewRow = Me.dtgAud.CurrentRow
        For Each row In dtgAud.Rows
            If row.Selected Then
                row.DefaultCellStyle.BackColor = Color.LightGreen
                Me.id = row.Cells(0).Value
                Me.Button1.Visible = True
                Select Case row.Cells(1).Value
                    Case "SISTEMAS"
                        Me.tipo_auditoria = "SISTEMA"
                    Case "PROCESOS"
                        Me.tipo_auditoria = "PROCESO"
                    Case "4MS"
                        Me.tipo_auditoria = "REPORTE ACCIONES CORRECTIVAS"
                End Select
                Me.file = "\\SERVER07\Software\Sistemas\Soluciones Visual Basic\Publicaciones\Control de Auditorias\Docs Base\" & Me.tipo_auditoria & ".xlsx"
            End If
            row.DefaultCellStyle.BackColor = Color.White
        Next
    End Sub

    Private Sub dt_del_ValueChanged(sender As Object, e As EventArgs) Handles dt_del.ValueChanged
        cambio_fecha()
    End Sub
    Private Sub dt_al_ValueChanged(sender As Object, e As EventArgs) Handles dt_al.ValueChanged
        cambio_fecha()
    End Sub

    Private Sub cambio_fecha()
        Try


            Dim row As DataRow
            If dt_del.Value <= dt_al.Value Then
                conexion = New Clss_ConexionSql
                Dim ds As DataSet
                Dim var = Me.cmbDep.SelectedValue.ToString
                If Me.cmbDep.SelectedValue.ToString = "001" Then
                    ds = conexion.selectTable("select ID, TIPO_AUDIT as Auditoria, ID_DEPART as Departamento, FECHA as Fecha, DESCR as Descripcion from [AUDI_PROG] where fecha between '" & Me.dt_del.Value.Date.Year & fecha(Me.dt_del.Value.Date.Month) & fecha(Me.dt_del.Value.Date.Day) & "' and '" & Me.dt_al.Value.Date.Year & fecha(Me.dt_al.Value.Date.Month) & fecha(Me.dt_al.Value.Date.Day) & "'")
                Else
                    ds = conexion.selectTable("select ID, TIPO_AUDIT as Auditoria, ID_DEPART as Departamento, FECHA as Fecha, DESCR as Descripcion from [AUDI_PROG] where ID_DEPART = '" & Me.cmbDep.SelectedValue.ToString & "' and (fecha between '" & Me.dt_del.Value.Date.Year & fecha(Me.dt_del.Value.Date.Month) & fecha(Me.dt_del.Value.Date.Day) & "' and '" & Me.dt_al.Value.Date.Year & fecha(Me.dt_al.Value.Date.Month) & fecha(Me.dt_al.Value.Date.Day) & "')")
                End If

                If ds IsNot Nothing Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            Dim MyString As String = ds.Tables(0).Rows(i).Item("Auditoria").ToString
                            Dim MyArray() As String = MyString.Split(";")
                            Dim MyList As List(Of String) = MyArray.ToList()
                            MyList.RemoveAt(MyList.Count - 1)
                            For j As Integer = 0 To MyList.Count - 1
                                If j = 0 Then
                                    ds.Tables(0).Rows(i).Item("Auditoria") = MyList.Item(j)
                                Else
                                    row = ds.Tables(0).NewRow
                                    row.Item(0) = ds.Tables(0).Rows(i).Item("ID")
                                    row.Item(1) = MyList.Item(j)
                                    row.Item(2) = ds.Tables(0).Rows(i).Item("Departamento")
                                    row.Item(3) = ds.Tables(0).Rows(i).Item("Fecha")
                                    row.Item(4) = ds.Tables(0).Rows(i).Item("Descripcion")
                                    ds.Tables(0).Rows.Add(row)
                                End If
                            Next
                        Next
                        dtgAud.DataSource = ds.Tables(0)
                    Else
                        dtgAud.DataSource = ds.Tables(0)
                    End If
                Else
                    conexion = New Clss_ConexionSql
                End If
                'dt_del.Value = dt_al.Value.Date.AddDays(-7)
            Else

                MsgBox("Fecha Incorrecta")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dtgAud_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgAud.CellContentDoubleClick
        'Me.dtgAud.Enabled = False
        'Me.file = "\\SERVER07\Software\Sistemas\Soluciones Visual Basic\Publicaciones\Control de Auditorias\Docs\" & Me.tipo_auditoria & ".xlsx"
        'Process.Start(file)
        'Me.Button1.Visible = True

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If IsFileOpen(Me.file) = False Then
            If MsgBox("¿Deseas Generar los Datos Iniciales de la Auditoria?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                guardar_actualizar_auditoria(Me.tipo_auditoria)
            End If
        Else
            MsgBox("El Documento Se Encuentra Abierto; LLenalo, Guardalo y Cierralo Para Poder Continuar...!!", MsgBoxStyle.Information)
        End If

    End Sub

    Private Function IsFileOpen(ByVal filePath As String) As Boolean
        Dim rtnvalue As Boolean = False
        Try
            Dim fs As System.IO.FileStream = System.IO.File.OpenWrite(filePath)
            fs.Close()
        Catch ex As System.IO.IOException
            rtnvalue = True
        End Try
        Return rtnvalue
    End Function
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Private Sub guardar_actualizar_auditoria(ByVal tipo As String)
        Select Case tipo
            Case "SISTEMA"
                Me.tipo_auditoria = "SISTEMA"
                Me.Frm_sistemas = New Frm_Sistema(Me.file)
                Me.Frm_sistemas.ShowDialog()
            Case "PROCESO"
                Me.tipo_auditoria = "PROCESO"
                Me.Frm_procesos = New Frm_Proceso(Me.file)
                Me.Frm_procesos.ShowDialog()
        End Select
    End Sub

    Private Sub cmbDep_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDep.SelectedIndexChanged
        cambio_fecha()
    End Sub
End Class